﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using COF.Server;
using ExitGames.Logging;
using COF.Base;
using System.Threading;
using COFBackendIntegration.BackendManager;
using static COFBackendIntegration.BackendManager.COFServerEnums;
using System.Collections;
using Photon.LoadBalancing.Operations;
using COFBackendIntegration.PhotonActions;

namespace Photon.Hive.Plugin.WebHooks
{
    public class COFSinglePlayer : PluginBase
    {

        #region Fields

        protected bool SuccesfullLoaded;

        private bool hasErrorEvents;

        private bool isPersistentFlag;

        private COFPlayerProfile player = new COFPlayerProfile();

        public DungeonState Dungeon = new DungeonState();

        private static readonly ILogger log = LogManager.GetCurrentClassLogger();

        private GameMechanicsSP gameLogicThread;

        public gameState gameState = COFServerEnums.gameState.initializing;

        #endregion

        #region Public Properties

        public override string Name
        {
            get
            {
                return "SP";
            }
        }

        #endregion

        public COFSinglePlayer()
        {
            this.UseStrictMode = true;
        }

        #region Public Methods and Operators

        public override void OnCloseGame(ICloseGameCallInfo info)
        {
            // Discard all game logics and data

            info.Continue();
        }

        async public override void OnCreateGame(ICreateGameCallInfo info)
        {
            String[] initDataFromName;
            // Initialize game
            info.Continue();
            try
            {
                // PlayfabId of player

                initDataFromName = this.PluginHost.GameId.Split('|');

                // TODO: this call is not working, eventually we should learn how to handle it.
                //player.playfabId = this.PluginHost.GameProperties["PI"].ToString();
                player.playfabId = initDataFromName[0];
                // Dungeon name
                // TODO: this call is not working, eventually we should learn how to handle it.
                //Dungeon.name = this.PluginHost.GameProperties["DN"].ToString();
                Dungeon.name = initDataFromName[1];
                log.Info("Player " + player.playfabId + " requests to play on dungeon " + Dungeon.name);

                player.ActorNr = 1;

                // Get player Inventory

                await BackendManager.Instance.GetInventory(player);

                // Get player's Deck

                await BackendManager.Instance.GetDeck(player);

                // Get dungeon data


                Dungeon.player = player;

                Dungeon.enemy = new COFPlayerProfile();

                // DEMO: enemy will share the same deck as the player
                Dungeon.enemy.deck = new List<ICatalogItem>(player.deck);

                // test dungeon for now: same deck as player



                // Set up setup Hands
                Dungeon.player.AddCardsOnHand(COFServerConsts.numberOfCardsOnHand);

                Dungeon.enemy.AddCardsOnHand(COFServerConsts.numberOfCardsOnHand);

                // Start game mechanics thread (with AI)
                gameLogicThread = new GameMechanicsSP(this);
                Thread newThread = new Thread(gameLogicThread.LogicRunner);
                newThread.Start();


            }
            catch (Exception e)
            {
                log.Error("Unable to initialize game " + info.Nickname + ". Error: " + e.Message);
                info.Fail(e.Message);
            }


            if (!info.IsJoin)
            {

            }
            else
            {
                //info.Continue();
            }


        }

        public override void OnJoin(IJoinGameCallInfo info)
        {
            // no sync OnJoin support for now - since we already changed the room state when we get here.
            // to allow a clean sync (cancelable OnJoin) we need to 
            // a. run all the pre-checks
            // b. call OnJoin with the expected new actornr
            // c. on continue - add peer to room 

            //if (!string.IsNullOrEmpty(this.gameJoinUrl))
            //{
            //    // remove for callAsync=false
            //    info.Continue();

            //    this.PostJsonRequest(
            //        this.gameJoinUrl,
            //        new WebhooksRequest
            //        {
            //            Type = "Join",
            //            GameId = this.PluginHost.GameId,
            //            AppId = this.AppId,
            //            AppVersion = this.AppVersion,
            //            Region = this.Region,
            //            UserId = info.UserId,
            //            Nickname = info.Nickname,
            //            ActorNr = info.ActorNr,
            //            AuthCookie = info.AuthCookie,
            //        },
            //        this.LogIfFailedCallback,
            //        info,
            //        callAsync: true);
            //}
            //else
            //{
            //    info.Continue();
            //}
        }

        public override void OnLeave(ILeaveGameCallInfo info)
        {
            base.OnLeave(info);

            //var url = this.gameLeaveUrl;
            //if (!string.IsNullOrEmpty(url)) // && info.ActorNr != -1 we don't restrict this anymore as in forwardplugin to see all leaves
            //{
            //    this.PostJsonRequest(
            //        url,
            //        new WebhooksRequest
            //        {
            //            Type = LeaveReason.ToString(info.Reason),
            //            GameId = this.PluginHost.GameId,
            //            AppId = this.AppId,
            //            AppVersion = this.AppVersion,
            //            Region = this.Region,
            //            UserId = info.UserId,
            //            Nickname = info.Nickname,
            //            ActorNr = info.ActorNr,
            //            IsInactive = info.IsInactive,
            //            Reason = info.Reason.ToString(),
            //            AuthCookie = WebFlags.ShouldSendAuthCookie(info.Request.WebFlags) ? info.AuthCookie : null,
            //        },
            //        this.LogIfFailedCallback,
            //        callAsync: true);
            //}
        }

        public override void OnRaiseEvent(IRaiseEventCallInfo info)
        {
            Dictionary<byte, object> eventData = new Dictionary<byte, object>();
            Dictionary<byte, object> Senddata = new Dictionary<byte, object>();
            IRaiseEventRequest raiseEventRequest = info.Request;

            List<COFPhotonAction> executionActions = null;
            List<Hashtable> serializedActions;

            Hashtable data = (Hashtable)raiseEventRequest.Data;

            switch (raiseEventRequest.EvCode)
            {
                case (byte)gameEventType.playerReady:

                    if (gameState == gameState.waitingForPlayers)
                    {
                        if (info.ActorNr == player.ActorNr)
                        {
                            player.status = playerConnectionStatus.ready;
                            log.Info("Player is ready");
                        }

                    }

                    break;

                case (byte)gameEventType.requestRedraw:

                    if (gameState == gameState.waitingForPlayers)
                    {
                        if (info.ActorNr == player.ActorNr)
                        {

                            string[] discardedCards = (string[])data[(byte)COFServerEnums.eventDataType.RedrawCards];

                            // execute card redraw. It could return a smaller number of cards if had problems
                            string[] addedCards = gameLogicThread.RedrawCards(info.ActorNr, discardedCards);

                            // register on Logs the entire operation

                            log.Info("Player requested to redraw cards :");

                            foreach (string singleLine in discardedCards)
                            {
                                log.Info(singleLine);
                            }

                            log.Info("New cards :");

                            if (addedCards != null)
                            {
                                foreach (string singleLine in addedCards)
                                {
                                    log.Info(singleLine);
                                }
                            }
                            else
                            {
                                log.Info("No cards has been added");
                            }

                            // if no cards has been redrawn, ignore the request
                            if (addedCards == null)
                                return;

                            // send event of new redraw

                            eventData.Add((byte)COFServerEnums.eventDataType.RedrawCards, addedCards);

                            Senddata.Add((byte)ParameterCode.Data, eventData);

                            PluginHost.BroadcastEvent(new int[] { info.ActorNr }, 0, (byte)gameEventType.ExecutedRedraw, Senddata, (byte)CacheOperations.DoNotCache);

                        }
                    }

                    break;

                case (byte)gameEventType.requestUseCard:

                    errorCodes executionResult = errorCodes.executionFailed;

                    PlayCardRequest cardRequested = (PlayCardRequest)data[(byte)COFServerEnums.eventDataType.PlayedCard];

                    //Hashtable cardData = (Hashtable) data[(byte)COFServerEnums.eventDataType.PlayedCard];

                    //PlayCardRequest cardRequested = new PlayCardRequest();

                    //cardRequested.instanceId = (string) cardData["I"];
                    //cardRequested.target = (COFServerEnums.battleTarget) cardData["T"];

                    if (gameState == gameState.running)
                    {
                        executionResult = gameLogicThread.ExecuteCard(info.ActorNr, cardRequested, out executionActions);
                    }

                    if (executionResult == errorCodes.none)
                    {
                        // send list of effects
                        if (executionActions.Count > 0)
                        {
                            serializedActions = new List<Hashtable>();

                            foreach (COFPhotonAction singleAction in executionActions)
                            {
                                serializedActions.Add(singleAction.ToHashtable());
                            }

                            eventData.Add((byte)eventDataType.photonActions, serializedActions.ToArray<Hashtable>());
                        }
                        // TODO: Register PhotonExecutionResult as serializable type
                        //eventData.Add((byte)COFServerEnums.eventDataType.ExecutionResult, new PhotonExecutionResult(true));
                        eventData.Add((byte)COFServerEnums.eventDataType.ExecutionResult, true);

                        Senddata.Add((byte)ParameterCode.Data, eventData);

                        PluginHost.BroadcastEvent(new int[] { info.ActorNr }, 0, (byte)gameEventType.ExecutedCard, Senddata, (byte)CacheOperations.DoNotCache);

                        log.Info("Card " + cardRequested.instanceId + " succesfully played by Actor " + info.ActorNr);
                    }
                    else
                    {
                        // send error message

                        eventData.Add((byte)COFServerEnums.eventDataType.ExecutionResult, false);
                        eventData.Add((byte)COFServerEnums.eventDataType.errorCode, executionResult);

                        Senddata.Add((byte)ParameterCode.Data, eventData);

                        PluginHost.BroadcastEvent(new int[] { info.ActorNr }, 0, (byte)gameEventType.ExecutedCard, Senddata, (byte)CacheOperations.DoNotCache);

                        log.Info("Card " + cardRequested.instanceId + " UNsuccesfully played by Actor " + info.ActorNr + ". Error code: " + executionResult);
                    }

                    // check if game is finished
                    if ((Dungeon.player.health <= 0) || (Dungeon.enemy.health <= 0))
                        gameState = gameState.finished;

                    break;

                default:

                    break;
            }

            base.OnRaiseEvent(info);


            //var url = this.gameEventUrl;

            //if (raiseEventRequest.HttpForward && !string.IsNullOrEmpty(url))
            //{
            //    var state = WebFlags.ShouldSendState(info.Request.WebFlags) ? this.GetGameState() : null;

            //    this.PostJsonRequest(
            //        url,
            //        new WebhooksRequest
            //        {
            //            Type = "Event",
            //            GameId = this.PluginHost.GameId,
            //            AppId = this.AppId,
            //            AppVersion = this.AppVersion,
            //            Region = this.Region,
            //            UserId = info.UserId,
            //            Nickname = info.Nickname,
            //            ActorNr = info.ActorNr,
            //            Data = raiseEventRequest.Data,
            //            State = state,
            //            AuthCookie = WebFlags.ShouldSendAuthCookie(info.Request.WebFlags) ? info.AuthCookie : null,
            //            EvCode = raiseEventRequest.EvCode,
            //        },
            //        this.LogIfFailedCallback,
            //        null,
            //        callAsync: !WebFlags.ShouldSendSync(info.Request.WebFlags));
            //}
        }

        private SerializableGameState GetGameState()
        {
            if (this.IsPersistent)
            {
                try
                {
                    return this.PluginHost.GetSerializableGameState();
                }
                catch (Exception ex)
                {
                    this.PluginHost.LogFatal(ex.Message);
                    throw;
                }
            }
            return null;
        }

        public override void OnSetProperties(ISetPropertiesCallInfo info)
        {
            base.OnSetProperties(info);

            //var setPropertiesRequest = info.Request;
            //var url = this.gamePropertiesUrl;

            //if (setPropertiesRequest.HttpForward && !string.IsNullOrEmpty(url))
            //{
            //    var state = WebFlags.ShouldSendState(info.Request.WebFlags) ? this.GetGameState() : null;

            //    this.PostJsonRequest(
            //        url,
            //        new WebhooksRequest
            //        {
            //            Type = info.Request.ActorNumber == 0 ? "Game" : "Actor",
            //            TargetActor = info.Request.ActorNumber == 0 ? null : (int?)info.Request.ActorNumber,
            //            GameId = this.PluginHost.GameId,
            //            AppId = this.AppId,
            //            AppVersion = this.AppVersion,
            //            Region = this.Region,
            //            UserId = info.UserId,
            //            Nickname = info.Nickname,
            //            ActorNr = info.ActorNr,
            //            Properties = setPropertiesRequest.Properties,
            //            State = state,
            //            AuthCookie = WebFlags.ShouldSendAuthCookie(info.Request.WebFlags) ? info.AuthCookie : null,
            //        },
            //        this.LogIfFailedCallback,
            //        null,
            //        callAsync: !WebFlags.ShouldSendSync(info.Request.WebFlags));
            //}
        }

        public override bool SetupInstance(IPluginHost host, Dictionary<string, string> config, out string errorMsg)
        {
            if (!base.SetupInstance(host, config, out errorMsg))
            {
                return false;
            }

            if (BackendManager.Instance.status != initState.initialized)
            {
                errorMsg = "Backend not initialized";
                return false;
            }

            //if (config != null && config.ContainsKey(BaseUrlKey))
            //{
            //    //                this.pluginConfig = config;

            //    this.baseUrl = this.GetKeyValue(config, BaseUrlKey);
            //    if (!string.IsNullOrEmpty(this.baseUrl))
            //    {
            //        this.isPersistentFlag = this.GetBoolKeyValue(config, GameIsPersistentKey);
            //        this.hasErrorEvents = this.GetBoolKeyValue(config, GameHasErrorEventsKey);

            //        this.baseUrl = this.baseUrl.Trim();

            //        this.gameCreatedUrl = this.GetUrl(config, GameCreateKey);
            //        this.gameClosedUrl = this.GetUrl(config, GameCloseKey);
            //        this.gameJoinUrl = this.GetUrl(config, GameJoinKey);
            //        this.gameLeaveUrl = this.GetUrl(config, GameLeaveKey);
            //        this.gameLoadUrl = this.GetUrl(config, GameLoadKey);
            //        this.gamePropertiesUrl = this.GetUrl(config, GamePropertiesKey);
            //        this.gameEventUrl = this.GetUrl(config, GameEventKey);
            //        var headers = this.GetKeyValue(config, CustomHttpHeadersKey);
            //        if (!string.IsNullOrEmpty(headers))
            //        {
            //            this.customHttpHeaders = JsonConvert.DeserializeObject<Dictionary<string, string>>(headers);
            //        }

            //        host.LogDebug(this.gameCreatedUrl);
            //        host.LogDebug(this.gameClosedUrl);
            //        host.LogDebug(this.gameEventUrl);

            //        //                    this.PluginHost.LogDebug("SetupInstance config:" + config.SerializeToString());
            //        return true;
            //    }
            //}

            //errorMsg = string.Format("Non null 'config' containing key '{0}' with non empty string value expected.", BaseUrlKey);
            return true;
        }

        #endregion

    }
}
