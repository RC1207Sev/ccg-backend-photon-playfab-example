﻿using ExitGames.Logging;
using Photon.LoadBalancing.Operations;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using static COFBackendIntegration.BackendManager.COFServerEnums;
using COFBackendIntegration.BackendManager;
using COF.Base;
using COF.Server;
using COFBackendIntegration.AI;
using COFBackendIntegration.PhotonActions;

namespace Photon.Hive.Plugin.WebHooks
{
    //!  Single Player game mechanics. 
    /*!
      This class will take care of the single player mechanics. 
      Class COFSinglePlayer will call the method LogicRunner which will run 
      for the entire life of the room.
    */
    public class GameMechanicsSP
    {
        private static readonly ILogger log = LogManager.GetCurrentClassLogger();
        COFSinglePlayer context;            /*!< this variable contain a copy of COFSinglePlayer wich contains all the information related to the specified room and dungeon */

        short secondsFromStart;

        //!  Constructor. 
        /*!
          \param A copy of all the data related to the associated room.
        */
        public GameMechanicsSP(COFSinglePlayer pContext)
        {
            context = pContext;
        }

        //! Card Redraw.
        /*!
          This method will remove a list of cards from player's hand and will replace the same amount of cards
          with newely randomly drawn cards. The discarded cards are then re-added to the deck.
            \param ActorNr the Actor Number of the player who requested the redraw
            \param discardedCards array of instanceIds of the cards to be discarded
            \return array of istanceIds of the redrawn cards
        */
        public string[] RedrawCards(int ActorNr, string[] discardedCards)
        {
            short removedCardsCount = 0;
            ICatalogItem SingleremovedCard;
            List<ICatalogItem> removedCards = new List<ICatalogItem>();
            List<string> addedCards = new List<string>();

            // if player has already redrawed cards, ignore request
            if (context.Dungeon.player.redrawedCards)
                return null;
            else
                context.Dungeon.player.redrawedCards = true;

            // remove cards on hand
            foreach (string singleCard in discardedCards)
            {
                // catch if one of the cards has not been removed
                SingleremovedCard = context.Dungeon.player.RemoveCardOnHand(singleCard, false);
                if (SingleremovedCard != null)
                {
                    removedCards.Add(SingleremovedCard);
                }
                else
                {
                    log.Error("Unable to remove card " + singleCard + " from hand");
                }
            }

            removedCardsCount = (short) removedCards.Count;

            // then redraw all removed cards
            for (int i=0; i<removedCardsCount; i++)
                addedCards.Add(context.Dungeon.player.AddCardsOnHand().itemInstanceID);

            // re-add removed cards to deck
            context.Dungeon.player.deck.AddRange(removedCards);

            // update room properties
            Hashtable initDataPlayer = new Hashtable();

            for (int i = 0; i < context.Dungeon.player.hand.Count; i++)
            {
                initDataPlayer.Add("D" + (i + 1), context.Dungeon.player.hand[i].itemInstanceID);
            }

            context.PluginHost.SetProperties(actorNr: ActorNr, properties: initDataPlayer, expected: null, broadcast: true);

            // return added cards
            return addedCards.ToArray<string>();

        }

        // if player is disconnected, stop the game
        // TODO: we should let the game goes on anyway
        public bool IsPlayerDisconnected()
        {
            if (context.PluginHost.GameActors.Count < 1)
            {
                return true;
            }

            return false;
        }

        // add a point of mana every 3 seconds
        private void ProcessManaRegeneration(short secondsFromStart)
        {
            if ((secondsFromStart > 0) && (secondsFromStart % COFServerConsts.secondsPerMana == 0))
            {
                if (context.Dungeon.enemy.mana <= COFServerConsts.maxMana)
                    context.PluginHost.SetProperties(actorNr: 0, properties: new Hashtable() { { "EM", context.Dungeon.enemy.mana++ } }, expected: null, broadcast: true); // actor=0 for Room properties
                if (context.Dungeon.player.mana <= COFServerConsts.maxMana)
                    context.PluginHost.SetProperties(actorNr: 1, properties: new Hashtable() { { "PM", context.Dungeon.player.mana++ } }, expected: null, broadcast: true);
            }
        }

        //! Game logic cycle.
        /*!
          This method will run for all the life of the game. It will cycle once every second,
          will execute enemy AI, combat results and health/mana regeneration.
            \return nothing
        */
        public async void LogicRunner()
        {

            log.Info("Entered Logic Runner");

            Dictionary<byte, object> data = new Dictionary<byte, object>();

            Dictionary<byte, object> eventData = new Dictionary<byte, object>();

            Hashtable initDataPlayer = new Hashtable();
            Hashtable initDataRoom = new Hashtable();

            List<Hashtable> serializedActions = new List<Hashtable>();

            List<COFPhotonAction> actionList = new List<COFPhotonAction>();

            AIManager currentAI = new AIManager(context.Dungeon);

            int delay = 0;

            battleTarget winner = battleTarget.none;

            // mana
            context.Dungeon.enemy.mana = 5;
            context.Dungeon.player.mana = 5;
            initDataPlayer.Add("PM", context.Dungeon.player.mana);
            initDataRoom.Add("EM", context.Dungeon.enemy.mana);

            // health
            context.Dungeon.player.health = COFServerConsts.maxHealth;
            context.Dungeon.enemy.health = COFServerConsts.maxHealth;
            initDataPlayer.Add("PH", context.Dungeon.player.health);
            initDataRoom.Add("EH", context.Dungeon.enemy.health);

            // graveyards
            initDataPlayer.Add("PG", 0); // player 
            initDataRoom.Add("EG", 0); //enemy 

            // Lineup data

            // player
            initDataPlayer.Add("I1", 0); // Id Creature
            initDataPlayer.Add("H1", 0); // Health
            initDataPlayer.Add("B1", 0); // Buffs/Debuffs
            initDataPlayer.Add("I2", 0);
            initDataPlayer.Add("H2", 0);
            initDataPlayer.Add("B2", 0);
            initDataPlayer.Add("I3", 0);
            initDataPlayer.Add("H3", 0);
            initDataPlayer.Add("B3", 0);
            initDataPlayer.Add("I4", 0);
            initDataPlayer.Add("H4", 0);
            initDataPlayer.Add("B4", 0);
            initDataPlayer.Add("I5", 0);
            initDataPlayer.Add("H5", 0);
            initDataPlayer.Add("B5", 0);

            // enemy
            initDataRoom.Add("I1", 0); // Id Creature
            initDataRoom.Add("H1", 0); // Health
            initDataRoom.Add("B1", 0); // Buffs/Debuffs
            initDataRoom.Add("I2", 0);
            initDataRoom.Add("H2", 0);
            initDataRoom.Add("B2", 0);
            initDataRoom.Add("I3", 0);
            initDataRoom.Add("H3", 0);
            initDataRoom.Add("B3", 0);
            initDataRoom.Add("I4", 0);
            initDataRoom.Add("H4", 0);
            initDataRoom.Add("B4", 0);
            initDataRoom.Add("I5", 0);
            initDataRoom.Add("H5", 0);
            initDataRoom.Add("B5", 0);

            // drawn cards

            //player
            for (int i=0; i<COFServerConsts.numberOfCardsOnHand; i++)
            {
                initDataPlayer.Add("D" + (i + 1), context.Dungeon.player.hand[i].itemInstanceID);
            }

            //enemy
            for (int i = 0; i < COFServerConsts.numberOfCardsOnHand; i++)
            {
                initDataRoom.Add("D" + (i + 1), context.Dungeon.enemy.hand[i].itemInstanceID);
            }

            secondsFromStart = 0;

            context.PluginHost.SetProperties(actorNr: 0, properties: initDataRoom, expected: null, broadcast: true); // actor=0 for Room properties
            context.PluginHost.SetProperties(actorNr: 1, properties: initDataPlayer, expected: null, broadcast: true);

            context.PluginHost.BroadcastEvent(new int[] { 1 }, 0, (byte)gameEventType.serverWaitingPlayers, data, (byte)CacheOperations.DoNotCache);

            log.Info("Game initialized, Waiting for Players @ " + DateTime.Now.ToString("h:mm:ss tt"));
            context.gameState = gameState.waitingForPlayers;
            // give time to properties to propagate
            //Thread.Sleep(500);

            // Waiting for all players to be ready. Server will wait a max amount of seconds if players are not ready,
            // and starts anyway
            for (int i=0; i<COFServerConsts.maxPlayerPreparationTimeInSeconds; i++)
            {
                if (context.Dungeon.player.status == playerConnectionStatus.ready)
                    break;
                Thread.Sleep(1000);
            }

            context.PluginHost.BroadcastEvent(new int[] { 1 }, 0, (byte)gameEventType.serverReady, data, (byte)CacheOperations.DoNotCache);
            log.Info("Game Started @ " + DateTime.Now.ToString("h:mm:ss tt"));

            context.gameState = gameState.running;

            //////////////////////////////////////////////////////////////////////////
            // Start of main cycle
            //////////////////////////////////////////////////////////////////////////

            for (int secs = 0; secs < COFServerConsts.maxGameLenghtInSeconds; secs++)
            {
                eventData.Clear();
                data.Clear();

                actionList.Clear();
                serializedActions.Clear();

                if (IsPlayerDisconnected())
                {
                    log.Info("Player disconnected: game closed");
                    return;
                }

                eventData.Add((byte)eventDataType.gameTime, secondsFromStart);

                // TODO what is this game over check for?
                if (context.gameState == gameState.finished)
                {
                    if (IsGameOver)
                        break;
                }

                // execute fights every end of turn
                if (secondsFromStart == COFServerConsts.secondsPerTurn)
                {
                    Hashtable playerLineupStatusHash = new Hashtable();
                    Hashtable EnemyLineupStatusHash = new Hashtable();

                    List<COFPhotonAction> fightActions;

                    // set a pause delay (in secs) to let the client process combat animations
                    delay = 3;

                    for (int i = 0; i < 5; i++)
                    {
                        int fightDelay = 0;

                        // Execute fight calculations for each lane
                        fightActions = context.Dungeon.Fight(context.Dungeon.TargetPositionFromLineupIndex(playingSide.player, i), context.Dungeon.TargetPositionFromLineupIndex(playingSide.enemy, i), out fightDelay);

                        delay += fightDelay;

                        // add generated actions to total
                        actionList.AddRange(fightActions);

                        // if the lane is empty, there's no need to delay for animations (1 sec per lane)
                        //if ((context.Dungeon.player.lineup[i] == null) && (context.Dungeon.enemy.lineup[i] == null))
                        //    delay--;
                    }

                    log.Info("Battle fight delay: " + delay + "s");

                    // updates photon properties by checking all generated photonActions
                    UpdatePhotonPropertiesFromPhotonActions(actionList);

                    // update graveyards
                    playerLineupStatusHash.Add("PG", context.Dungeon.player.graveyard.Count);
                    EnemyLineupStatusHash.Add("EG", context.Dungeon.enemy.graveyard.Count);

                    context.PluginHost.SetProperties(actorNr: 0, properties: EnemyLineupStatusHash, expected: null, broadcast: true); // actor=0 for Room properties
                    context.PluginHost.SetProperties(actorNr: 1, properties: playerLineupStatusHash, expected: null, broadcast: true);
                }

                if (!IsGameOver)
                {
                    ProcessManaRegeneration(secondsFromStart);

                    // ProcessDeckDraw
                    if (secs > 0 && ((secondsFromStart == 0) || (secondsFromStart == COFServerConsts.secondsPerCard)))
                    {
                        ICatalogItem drawnCard;

                        // add card to players hand
                        if (context.Dungeon.player.hand.Count < COFServerConsts.numberOfCardsOnHand)
                        {
                            drawnCard = context.Dungeon.player.AddCardsOnHand();
                            if (drawnCard != null)
                            {
                                actionList.Add(new COFPhotonActionCardDrawn(COFServerEnums.battleTarget.player, COFServerEnums.battleTarget.player, drawnCard.itemID, drawnCard.itemInstanceID));
                            }
                        }

                        if (context.Dungeon.enemy.hand.Count < COFServerConsts.numberOfCardsOnHand)
                        {
                            drawnCard = context.Dungeon.enemy.AddCardsOnHand();
                            if (drawnCard != null)
                            {
                                actionList.Add(new COFPhotonActionCardDrawn(COFServerEnums.battleTarget.enemy, COFServerEnums.battleTarget.enemy, drawnCard.itemID, drawnCard.itemInstanceID));
                            }
                        }

                    }

                    // ProcessAI
                    if ((secondsFromStart > 0) && (secondsFromStart % COFServerConsts.secondsPerAIDecision == 0))
                    {
                        AIAction enemyAction;

                        enemyAction = currentAI.GetCardToPlay();

                        if (enemyAction != null)
                        {
                            List<COFPhotonAction> AIactions = new List<COFPhotonAction>();
                            // execute the card
                            // TODO: this will work only with simple actions (doesn't involve a second target)
                            errorCodes executionResult = ExecuteCard(0, new PlayCardRequest(enemyAction.inventoryIdCardToUse, enemyAction.target1), out AIactions);
                            // add PhotonAction
                            if (executionResult == errorCodes.none)
                            {
                                actionList.AddRange(AIactions);
                            }
                        }
                    }
                }

                eventData.Add((byte)eventDataType.gameState, context.gameState);

                // ProcessPhotonActions
                if (actionList.Count > 0)
                {
                    foreach (COFPhotonAction singleAction in actionList)
                    {
                        serializedActions.Add(singleAction.ToHashtable());
                    }

                    eventData.Add((byte)eventDataType.photonActions, serializedActions.ToArray<Hashtable>());
                }

                data.Add((byte)ParameterCode.Data, eventData);

                // finally raise the event to all players, announce if last cycle in a turn
                if (secondsFromStart == COFServerConsts.secondsPerTurn)
                {
                    context.PluginHost.BroadcastEvent(new int[] { 1 }, 0, (byte)gameEventType.endOfTurn, data, (byte)CacheOperations.DoNotCache);
                }
                else
                {
                    context.PluginHost.BroadcastEvent(new int[] { 1 }, 0, (byte)gameEventType.cycleUpdate, data, (byte)CacheOperations.DoNotCache);
                }

                // system to pause the game, give the client secs to elaborate endOfTurn animations
                if (delay > 0)
                {
                    eventData.Clear();
                    data.Clear();

                    context.gameState = gameState.paused;

                    // inform the players that the game state is now paused
                    eventData.Add((byte)eventDataType.gameState, gameState.paused);
                    data.Add((byte)ParameterCode.Data, eventData);
                    context.PluginHost.BroadcastEvent(new int[] { 1 }, 0, (byte)gameEventType.gameStateChanged, data, (byte)CacheOperations.DoNotCache);
                    log.Info("Game paused for " + delay + " seconds");

                    // wait for the specified amount of time
                    Thread.Sleep(1000 * delay);

                    // inform the players that the game state is now running again
                    secondsFromStart = 0;
                    eventData[(byte)eventDataType.gameState] = gameState.running;
                    context.PluginHost.BroadcastEvent(new int[] { 1 }, 0, (byte)gameEventType.gameStateChanged, data, (byte)CacheOperations.DoNotCache);
                    log.Info("Game resumed");

                    delay = 0;
                    context.gameState = gameState.running;
                }
                else
                {
                    Thread.Sleep(1000);
                    secondsFromStart++;
                }

                if (IsGameOver)
                    break;
            }

            eventData.Clear();
            data.Clear();

            context.gameState = gameState.finished;
            
            eventData.Add((byte)eventDataType.gameState, context.gameState);
            data.Add((byte)ParameterCode.Data, eventData);

            // we send end of game information now since playfab's webAPI is slow.
            context.PluginHost.BroadcastEvent(new int[] { 1 }, 0, (byte)gameEventType.gameStateChanged, data, (byte)CacheOperations.DoNotCache);

            data.Clear();
            eventData.Clear();

            string reward;

            winner = GetWinner();
            switch (winner)
            {
                case battleTarget.player:
                    // get reward from playfab
                    reward = await BackendManager.Instance.AddReward(context.Dungeon.player, context.Dungeon);
                    eventData.Add((byte)eventDataType.reward, reward);
                    break;
                case battleTarget.enemy:
                    break;
                case battleTarget.none:
                    break;
                default:
                    // something wrong here
                    break;
            }

            eventData.Add((byte)eventDataType.winner, winner);
            data.Add((byte)ParameterCode.Data, eventData);
            // once we have the potential reward, we send the endOfGame
            context.PluginHost.BroadcastEvent(new int[] { 1 }, 0, (byte)gameEventType.endOfGame, data, (byte)CacheOperations.DoNotCache);

        }

        //! Card execution.
        /*!
          This method will execute the effect of a given Card.
          It will first check all the conditions for card use, execute the effect and remove the card from hand.
            \param requester the Actor Number of the player who requested to use the card
            \param requestedCard data about the requested card (see PlayCardRequest for more info)
            \return the error code generated by the execution (if it is succesfull, it will return errorCodes.none)
        */
        public errorCodes ExecuteCard(int requester, PlayCardRequest requestedCard, out List<COFPhotonAction> executionActions)
        {
            
            ICatalogItem card;
            COFPlayerProfile playerActor;
            LineupPosition battlefieldSlot = LineupPosition.topLane_1;

            executionActions = new List<COFPhotonAction>();

            // check from who this requests come and check the hand
            if (requester == context.Dungeon.player.ActorNr)
            {
                playerActor = context.Dungeon.player;
            }
            else // only alternative is AI
            {
                playerActor = context.Dungeon.enemy;
            }
            card = isCardInstanceIdInHand(playerActor.hand, requestedCard.instanceId);

            if (card == null)
                return errorCodes.cardNotFound;

            // check if target is acceptable for target's effect
            if (card.catalogItemType == typeof(CatalogCardWeapon))
            {
                // valid targets are only own battlefield filled slots
                if (requester == context.Dungeon.player.ActorNr)
                {
                    switch (requestedCard.target)
                    {
                        case battleTarget.position1_a:
                            battlefieldSlot = LineupPosition.topLane_1;
                            break;
                        case battleTarget.position2_a:
                            battlefieldSlot = LineupPosition.topLane_2;
                            break;
                        case battleTarget.position3_a:
                            battlefieldSlot = LineupPosition.midLane;
                            break;
                        case battleTarget.position4_a:
                            battlefieldSlot = LineupPosition.bottomLane_1;
                            break;
                        case battleTarget.position5_a:
                            battlefieldSlot = LineupPosition.bottomLane_2;
                            break;

                        default: // any other target is invalid
                            return errorCodes.invalidTarget;
                    }
                }
                else  // only alternative for now is computer AI
                {
                    switch (requestedCard.target)
                    {
                        case battleTarget.position1_b:
                            battlefieldSlot = LineupPosition.topLane_1;
                            break;
                        case battleTarget.position2_b:
                            battlefieldSlot = LineupPosition.topLane_2;
                            break;
                        case battleTarget.position3_b:
                            battlefieldSlot = LineupPosition.midLane;
                            break;
                        case battleTarget.position4_b:
                            battlefieldSlot = LineupPosition.bottomLane_1;
                            break;
                        case battleTarget.position5_b:
                            battlefieldSlot = LineupPosition.bottomLane_2;
                            break;

                        default: // any other target is invalid
                            return errorCodes.invalidTarget;
                    }
                }

                // check if the position is occupied
                if (playerActor.lineup[(int)battlefieldSlot] == null)
                    return errorCodes.invalidTarget;

            }

            // check if target is acceptable for target's effect
            // switch (card.catalogItemType) TODO: ASAP!!!!!! change from Type to Enum
            if (card.catalogItemType == typeof(CatalogCardCreature))
            {
                // if creature, valid targets are only own battlefield empty slots
                if (requester == context.Dungeon.player.ActorNr)
                {
                    switch (requestedCard.target)
                    {
                        case battleTarget.position1_a:
                            battlefieldSlot = LineupPosition.topLane_1;
                            break;
                        case battleTarget.position2_a:
                            battlefieldSlot = LineupPosition.topLane_2;
                            break;
                        case battleTarget.position3_a:
                            battlefieldSlot = LineupPosition.midLane;
                            break;
                        case battleTarget.position4_a:
                            battlefieldSlot = LineupPosition.bottomLane_1;
                            break;
                        case battleTarget.position5_a:
                            battlefieldSlot = LineupPosition.bottomLane_2;
                            break;

                        default: // any other target is invalid
                            return errorCodes.invalidTarget;
                    }
                }
                else  // only alternative for now is computer AI
                {
                    switch (requestedCard.target)
                    {
                        case battleTarget.position1_b:
                            battlefieldSlot = LineupPosition.topLane_1;
                            break;
                        case battleTarget.position2_b:
                            battlefieldSlot = LineupPosition.topLane_2;
                            break;
                        case battleTarget.position3_b:
                            battlefieldSlot = LineupPosition.midLane;
                            break;
                        case battleTarget.position4_b:
                            battlefieldSlot = LineupPosition.bottomLane_1;
                            break;
                        case battleTarget.position5_b:
                            battlefieldSlot = LineupPosition.bottomLane_2;
                            break;

                        default: // any other target is invalid
                            return errorCodes.invalidTarget;
                    }
                }

                // check if the position is occupied
                if (playerActor.lineup[(int)battlefieldSlot] != null)
                    return errorCodes.invalidTarget;

            }

            // check mana
            if (playerActor.mana < ((CatalogBaseCard)card).manaCost)
                return errorCodes.notEnoughMana;

            playingSide playerOrAI = playerActor.ActorNr == 1 ? playingSide.player : playingSide.enemy;

            // Card execution
            if (card.catalogItemType == typeof(CatalogCardWeapon))
            {
                AddEquipOnCreatureInPosition(playerActor, (int)battlefieldSlot, (CatalogCardWeapon)card);
                
                executionActions.Add(new COFPhotonActionNewEquipment(playerActor.ActorNr == 1 ? battleTarget.player : battleTarget.enemy,
                                                                    context.Dungeon.TargetPositionFromLineupIndex(playerOrAI, (int)battlefieldSlot),
                                                                    card.itemID));
                log.Info("Added new equipment card " + card.itemID + " to creature in " + context.Dungeon.TargetPositionFromLineupIndex(playerOrAI, (int)battlefieldSlot).ToString());
                // remove card from hand
                playerActor.RemoveCardOnHand(card.itemInstanceID);

            }

            if (card.catalogItemType == typeof(CatalogCardSpell))
            {
                log.Info("Added new spell card " + card.itemID + " to creature in " + context.Dungeon.TargetPositionFromLineupIndex(playerOrAI, (int)battlefieldSlot).ToString());
                // remove card from hand
                playerActor.RemoveCardOnHand(card.itemInstanceID);

            }

            if (card.catalogItemType == typeof(CatalogCardCreature))
            {
                PlayCreatureOnPosition(playerActor, (int)battlefieldSlot, (CatalogCardCreature)card);

                // add photonAction
                // TODO: TEST!!!
                executionActions.Add(new COFPhotonActionNewCreature(playerActor.ActorNr == 1 ? battleTarget.player : battleTarget.enemy,
                                                                    context.Dungeon.TargetPositionFromLineupIndex(playerOrAI, (int)battlefieldSlot),
                                                                    card.itemID));

                log.Info("Added new creature card " + card.itemID + " to creature in " + context.Dungeon.TargetPositionFromLineupIndex(playerOrAI, (int)battlefieldSlot).ToString());

                // remove card from hand (no graveyard yet if Creature)
                playerActor.RemoveCardOnHand(card.itemInstanceID, false);
            }

            // execute card skill on impact
            if (((CatalogBaseCard)card).cardSkill.skillType != cardSkill.none)
                executionActions.AddRange(COFSkill.Execute(playerActor, context.Dungeon, (CatalogBaseCard)card, requestedCard.target, cardSkillExecutionTime.impact));

            // update room/player properties after skill effects
            Hashtable drawnCardsOnHand = new Hashtable();
            Hashtable roomProperties = new Hashtable();

            UpdatePhotonPropertiesFromPhotonActions(executionActions);

            // pay mana
            playerActor.mana -= ((CatalogBaseCard)card).manaCost;

            // update drawn cards room properties

            for (int i = 0; i < COFServerConsts.numberOfCardsOnHand; i++)
            {
                if (i < playerActor.hand.Count)
                    drawnCardsOnHand.Add("D" + (i + 1), playerActor.hand[i].itemInstanceID);
                else
                    drawnCardsOnHand.Add("D" + (i + 1), 0);
            }

            // update card player mana room property
            if (playerActor.ActorNr == context.Dungeon.player.ActorNr)
            {
                drawnCardsOnHand.Add("PM", playerActor.mana);
            }
            else
            {
                drawnCardsOnHand.Add("EM", playerActor.mana);
            }

            context.PluginHost.SetProperties(actorNr: requester, properties: drawnCardsOnHand, expected: null, broadcast: true);
            context.PluginHost.SetProperties(actorNr: 0, properties: roomProperties, expected: null, broadcast: true);

            return errorCodes.none;

        }

        private void UpdatePhotonPropertiesFromPhotonActions(List<COFPhotonAction> actions)
        {
            // update room/player properties after skill effects
            Hashtable drawnCardsOnHand = new Hashtable();
            Hashtable roomProperties = new Hashtable();

            PlayingCreature tempCreature;
            
            foreach (COFPhotonAction singleAction in actions)
            {
                switch (singleAction.ActionType)
                {
                    case photonActionType.healing:
                    case photonActionType.creatureDamage:

                        COFPhotonActionCreatureDamage damageAction = (COFPhotonActionCreatureDamage)singleAction;

                        if ((damageAction.targetPosition == battleTarget.player) || (damageAction.targetPosition == battleTarget.enemy))
                        {
                            // update player/enemy health
                            if (damageAction.targetPosition == battleTarget.player)
                            {
                                if (!drawnCardsOnHand.Contains("PH"))
                                    drawnCardsOnHand.Add("PH", context.Dungeon.player.health);
                            }
                            else
                            {
                                if (!roomProperties.Contains("EH"))
                                    roomProperties.Add("EH", context.Dungeon.enemy.health);
                            }
                        }
                        else
                        {
                            tempCreature = context.Dungeon.CreatureFromBattleTarget(damageAction.targetPosition);

                            // TODO: move all lineups on room properties only
                            if (tempCreature != null)
                            {
                                if (context.Dungeon.player == tempCreature.Owner)
                                {
                                    if ((tempCreature != null) && (!drawnCardsOnHand.Contains("H" + (tempCreature.Position + 1))))
                                        drawnCardsOnHand.Add("H" + (tempCreature.Position + 1), tempCreature.Health);
                                }
                                else
                                {
                                    if ((tempCreature != null) && (!roomProperties.Contains("H" + (tempCreature.Position + 1))))
                                        roomProperties.Add("H" + (tempCreature.Position + 1), tempCreature.Health);
                                }
                            }

                        }

                        log.Info(damageAction.sourcePosition.ToString() + " damages " + damageAction.targetPosition.ToString() + ": " + damageAction.amount + " hp");

                        break;

                    case photonActionType.changeMana:

                        COFPhotonActionChangeMana manaAction = (COFPhotonActionChangeMana)singleAction;

                        // update player/enemy health
                        if (manaAction.targetPosition == battleTarget.player)
                        {
                            if (!drawnCardsOnHand.Contains("PM"))
                                drawnCardsOnHand.Add("PM", context.Dungeon.player.health);
                        }
                        else
                        {
                            if (!roomProperties.Contains("EM"))
                                roomProperties.Add("EM", context.Dungeon.enemy.health);
                        }

                        log.Info(manaAction.targetPosition.ToString() + " receives " + manaAction.amount + " mana from " + manaAction.sourcePosition.ToString());

                        break;

                    case photonActionType.creatureDeath:

                        COFPhotonActionCreatureDeath deathAction = (COFPhotonActionCreatureDeath)singleAction;

                        int creaturePosition;

                        tempCreature = context.Dungeon.CreatureFromBattleTarget(deathAction.creaturePosition);

                        // TODO: move all lineups on room properties only
                        if (tempCreature != null)
                        {
                            // add to graveyard
                            tempCreature.Owner.graveyard.Add(tempCreature.Card);

                            creaturePosition = tempCreature.Position + 1;

                        }
                        else
                        {
                            creaturePosition = context.Dungeon.GetIndexPositionFromBattlePosition(deathAction.creaturePosition) + 1;
                        }

                        if (context.Dungeon.player == context.Dungeon.GetPlayerOwnerFromBattlePosition(deathAction.creaturePosition))
                        {
                            if (!drawnCardsOnHand.Contains("H" + creaturePosition))
                                drawnCardsOnHand.Add("H" + creaturePosition, 0);
                            else
                                drawnCardsOnHand["H" + creaturePosition] = tempCreature.Health;

                            drawnCardsOnHand.Add("I" + creaturePosition, 0);
                            drawnCardsOnHand.Add("B" + creaturePosition, 0);

                        }
                        else
                        {
                            if (!roomProperties.Contains("H" + creaturePosition))
                                roomProperties.Add("H" + creaturePosition, 0);
                            else
                                roomProperties["H" + creaturePosition] = tempCreature.Health;

                            roomProperties.Add("I" + creaturePosition, 0);
                            roomProperties.Add("B" + creaturePosition, 0);
                        }

                        log.Info(deathAction.creaturePosition + " dies");

                        break;

                    case photonActionType.addedCardtoHand:

                        COFPhotonActionCardDrawn DrawAction = (COFPhotonActionCardDrawn)singleAction;

                        log.Info(DrawAction.targetPosition.ToString() + " receives card " + DrawAction.cardId + " [" + DrawAction.cardInstanceId + "]");
                        break;

                    case photonActionType.addedStatus:

                        COPhotonActionAddedStatus statusAction = (COPhotonActionAddedStatus)singleAction;

                        log.Info(statusAction.targetPosition.ToString() + " receives status " + statusAction.status.ToString());
                        break;

                    case photonActionType.addedEquipment:

                        COFPhotonActionNewEquipment EquipAction = (COFPhotonActionNewEquipment)singleAction;

                        log.Info(EquipAction.targetPosition.ToString() + " receives weapon " + EquipAction.cardId);
                        break;

                    case photonActionType.creatureAdded:

                        COFPhotonActionNewCreature AddCreatureAction = (COFPhotonActionNewCreature)singleAction;

                        log.Info("creature " + AddCreatureAction.cardId +  " added in position " + AddCreatureAction.targetPosition.ToString());

                        break;

                    default:
                        break;
                }
            }

            context.PluginHost.SetProperties(actorNr: context.Dungeon.player.ActorNr, properties: drawnCardsOnHand, expected: null, broadcast: true);
            context.PluginHost.SetProperties(actorNr: context.Dungeon.enemy.ActorNr, properties: roomProperties, expected: null, broadcast: true);

        }


        private bool IsGameOver
        {
            get
            {
                if (context.Dungeon.player.health <= 0)
                    return true;

                if (context.Dungeon.enemy.health <= 0)
                    return true;

                return false;
            }
        }

        private battleTarget GetWinner()
        {
            // if player wins
            if ((context.Dungeon.enemy.health <= 0) && (context.Dungeon.player.health > 0))
            {
                return battleTarget.player;
            }
            // if enemy wins
            if ((context.Dungeon.player.health <= 0) && (context.Dungeon.enemy.health > 0))
            {
                return battleTarget.enemy;
            }
            // if tie
            if ((context.Dungeon.player.health <= 0) && (context.Dungeon.enemy.health <= 0))
            {
                return battleTarget.none;
            }

            return battleTarget.none;
        }

        //! Add equipment to a specified creature on the battlefield.
        /*!
          It will add the card to the creature (must check first if creature exists) 
          and then will update player's properties
            \param requester the Actor Number of the player who requested to use the card
            \param requestedCard data about the requested card (see PlayCardRequest for more info)
            \return the error code generated by the execution (if it is succesfull, it will return errorCodes.none)
        */
        private void AddEquipOnCreatureInPosition(COFPlayerProfile Actor, int position, CatalogCardWeapon card)
        {
            Hashtable creatureData = new Hashtable();

            Actor.lineup[position].AddEquipment(card);

            creatureData.Add("H" + (position + 1), Actor.lineup[position].Health);

            context.PluginHost.SetProperties(actorNr: Actor.ActorNr, properties: creatureData, expected: null, broadcast: true);
        }

        //! Add creature on specified battlefield position.
        /*!
          This method will execute the effect of a given Card.
          It will fist check all the conditions for card use, execute the effect and remove the card from hand.
            \param requester the Actor Number of the player who requested to use the card
            \param requestedCard data about the requested card (see PlayCardRequest for more info)
            \return the error code generated by the execution (if it is succesfull, it will return errorCodes.none)
        */
        private void PlayCreatureOnPosition(COFPlayerProfile Actor, int position, CatalogCardCreature card)
        {
            Hashtable creatureData = new Hashtable();

            Actor.lineup[position] = new PlayingCreature(Actor, (short)position, card.itemInstanceID, (CatalogCardCreature)card, ((CatalogCardCreature)card).HP, null);

            creatureData.Add("I" + (position + 1), Actor.ActorNr == 0 ? card.itemID : card.itemInstanceID);
            creatureData.Add("H" + (position + 1), card.HP);
            creatureData.Add("B" + (position + 1), 0);

            context.PluginHost.SetProperties(actorNr: Actor.ActorNr, properties: creatureData, expected: null, broadcast: true);

        }

        //! Check's if the card with given InstanceId is in the given hand (list of ICatalogItem).
        /*!
            \param hand list of ICataloItem to check agains. It should be the hand of one of the players
            \param instanceId the string of InstanceId of the card to search
            \return the ICatalogItem found (returns null if not found)
        */
        private static ICatalogItem isCardInstanceIdInHand(ICollection<ICatalogItem> hand, string instanceId)
        {
            foreach (ICatalogItem singleCard in hand)
            {
                if (singleCard.itemInstanceID == instanceId)
                    return singleCard;
            }

            return null;
        }

    }
}
