﻿using ExitGames.Logging;
using Photon.LoadBalancing.Operations;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using static COFBackendIntegration.BackendManager.COFServerEnums;
using COFBackendIntegration.BackendManager;
using COF.Base;
using COF.Server;

namespace Photon.Hive.Plugin.WebHooks
{

    public class GameMechanicsSP
    {
        private static readonly ILogger log = LogManager.GetCurrentClassLogger();
        COFSinglePlayer context;

        short secondsFromStart;

        public GameMechanicsSP(COFSinglePlayer pContext)
        {
            context = pContext;
        }

        public string[] RedrawCards(int ActorNr, string[] discardedCards)
        {
            short removedCardsCount = 0;
            ICatalogItem SingleremovedCard;
            List<ICatalogItem> removedCards = new List<ICatalogItem>();
            List<string> addedCards = new List<string>();

            // if player has already redrawed cards, ignore request
            if (context.Dungeon.player.redrawedCards)
                return null;
            else
                context.Dungeon.player.redrawedCards = true;

            // remove cards on hand
            foreach (string singleCard in discardedCards)
            {
                // catch if one of the cards has not been removed
                SingleremovedCard = context.Dungeon.player.RemoveCardOnHand(singleCard, false);
                if (SingleremovedCard != null)
                {
                    removedCards.Add(SingleremovedCard);
                }
                else
                {
                    log.Error("Unable to remove card " + singleCard + " from hand");
                }
            }

            removedCardsCount = (short) removedCards.Count;

            // then redraw all removed cards
            for (int i=0; i<removedCardsCount; i++)
                addedCards.Add(context.Dungeon.player.AddCardsOnHand().itemInstanceID);

            // read removed cards to deck
            context.Dungeon.player.deck.AddRange(removedCards);

            // update room properties
            Hashtable initDataPlayer = new Hashtable();

            for (int i = 0; i < context.Dungeon.player.hand.Count; i++)
            {
                initDataPlayer.Add("D" + (i + 1), context.Dungeon.player.hand[i].itemInstanceID);
            }

            context.PluginHost.SetProperties(actorNr: ActorNr, properties: initDataPlayer, expected: null, broadcast: true);

            // return added cards
            return addedCards.ToArray<string>();

        }

        public void LogicRunner()
        {

            log.Info("Entered Logic Runner");

            Dictionary<byte, object> data = new Dictionary<byte, object>();

            Dictionary<byte, object> eventData = new Dictionary<byte, object>();

            Hashtable initDataPlayer = new Hashtable();
            Hashtable initDataRoom = new Hashtable();

            // mana
            initDataPlayer.Add("PM", 5); //player
            initDataRoom.Add("EM", 5); //enemy 

            // health
            initDataPlayer.Add("PH", 30); //player
            initDataRoom.Add("EH", 30); //enemy

            // graveyards
            initDataPlayer.Add("PG", 0); // player 
            initDataRoom.Add("EG", 0); //enemy 

            // Lineup data

            // player
            initDataPlayer.Add("I1", 0); // Id Creature
            initDataPlayer.Add("H1", 0); // Health
            initDataPlayer.Add("B1", 0); // Buffs/Debuffs
            initDataPlayer.Add("I2", 0);
            initDataPlayer.Add("H2", 0);
            initDataPlayer.Add("B2", 0);
            initDataPlayer.Add("I3", 0);
            initDataPlayer.Add("H3", 0);
            initDataPlayer.Add("B3", 0);
            initDataPlayer.Add("I4", 0);
            initDataPlayer.Add("H4", 0);
            initDataPlayer.Add("B4", 0);
            initDataPlayer.Add("I5", 0);
            initDataPlayer.Add("H5", 0);
            initDataPlayer.Add("B5", 0);

            // enemy
            initDataRoom.Add("I1", 0); // Id Creature
            initDataRoom.Add("H1", 0); // Health
            initDataRoom.Add("B1", 0); // Buffs/Debuffs
            initDataRoom.Add("I2", 0);
            initDataRoom.Add("H2", 0);
            initDataRoom.Add("B2", 0);
            initDataRoom.Add("I3", 0);
            initDataRoom.Add("H3", 0);
            initDataRoom.Add("B3", 0);
            initDataRoom.Add("I4", 0);
            initDataRoom.Add("H4", 0);
            initDataRoom.Add("B4", 0);
            initDataRoom.Add("I5", 0);
            initDataRoom.Add("H5", 0);
            initDataRoom.Add("B5", 0);

            // drawn cards

            //player
            for (int i=0; i<COFServerConsts.numberOfCardsOnHand; i++)
            {
                initDataPlayer.Add("D" + (i + 1), context.Dungeon.player.hand[i].itemInstanceID);
            }

            //enemy
            for (int i = 0; i < COFServerConsts.numberOfCardsOnHand; i++)
            {
                initDataRoom.Add("D" + (i + 1), context.Dungeon.enemy.hand[i].itemInstanceID);
            }

            secondsFromStart = 0;

            context.PluginHost.SetProperties(actorNr: 0, properties: initDataRoom, expected: null, broadcast: true); // actor=0 for Room properties
            context.PluginHost.SetProperties(actorNr: 1, properties: initDataPlayer, expected: null, broadcast: true);

            context.PluginHost.BroadcastEvent(new int[] { 1 }, 0, (byte)gameEventType.serverWaitingPlayers, data, (byte)CacheOperations.DoNotCache);

            log.Info("Game initialized, Waiting for Players");
            context.gameState = gameState.waitingForPlayers;
            // give time to properties to propagate
            Thread.Sleep(500);

            // Waiting for all players to be ready. Server will wait a max amount of seconds if players are not ready,
            // and starts anyway
            for (int i=0; i<COFServerConsts.maxPlayerPreparationTimeInSeconds; i++)
            {
                if (context.Dungeon.player.status == playerConnectionStatus.ready)
                    break;
                Thread.Sleep(1000);
            }

            context.PluginHost.BroadcastEvent(new int[] { 1 }, 0, (byte)gameEventType.serverReady, data, (byte)CacheOperations.DoNotCache);
            log.Info("Game Started");

            context.Dungeon.enemy.mana = 5;
            context.Dungeon.player.mana = 5;

            context.Dungeon.enemy.health = 30;
            context.Dungeon.player.health = 30;

            context.gameState = gameState.running;

            // Start of main cycle

            for (int secs=0; secs<COFServerConsts.maxGameLenghtInSeconds; secs++)
            {
                Thread.Sleep(1000);

                // if player is disconnected, stop the game
                // TODO: we should let the game goes on anyway
                if (context.PluginHost.GameActors.Count < 1)
                {
                    log.Info("Player disconnected: game closed");
                    return;
                }
                
                eventData.Clear();
                data.Clear();

                secondsFromStart++;

                // execute fights every end of turn
                if (secondsFromStart % COFServerConsts.secondsPerTurn == 0)
                {
                    Hashtable playerLineupStatusHash = new Hashtable();
                    Hashtable EnemyLineupStatusHash = new Hashtable();

                    // TODO: execute all damages for all lanes
                    PlayingCreature currentPlayerCreature, currentEnemyCreature;

                    for (int i=0; i<5; i++)
                    {
                        currentPlayerCreature = context.Dungeon.player.lineup[i];
                        currentEnemyCreature = context.Dungeon.enemy.lineup[i];

                        // If player Creature is missing, enemy creature will attack enemy hero
                        // otherwise it will damage enemy Creature
                        if (currentPlayerCreature != null)
                        {
                            if (currentEnemyCreature != null) // both creatures are present
                            {
                                currentPlayerCreature.Health -= ((CatalogCardCreature)currentEnemyCreature.Card).attack;
                                currentEnemyCreature.Health -= ((CatalogCardCreature)currentPlayerCreature.Card).attack;

                                // remove dead Creatures
                                if (currentPlayerCreature.Health <= 0)
                                {
                                    // add to graveyard
                                    context.Dungeon.player.graveyard.Add(context.Dungeon.player.lineup[i].Card);

                                    playerLineupStatusHash.Add("I" + (i + 1), 0);
                                    playerLineupStatusHash.Add("H" + (i + 1), 0);
                                    playerLineupStatusHash.Add("B" + (i + 1), 0);

                                    context.Dungeon.player.lineup[i] = null;

                                }
                                else // otherwise just update Health and buffs
                                {
                                    playerLineupStatusHash.Add("H" + (i + 1), currentPlayerCreature.Health);
                                }

                                if (currentEnemyCreature.Health <= 0)
                                {
                                    // add to graveyard
                                    context.Dungeon.enemy.graveyard.Add(context.Dungeon.enemy.lineup[i].Card);

                                    EnemyLineupStatusHash.Add("I" + (i + 1), 0);
                                    EnemyLineupStatusHash.Add("H" + (i + 1), 0);
                                    EnemyLineupStatusHash.Add("B" + (i + 1), 0);

                                    context.Dungeon.enemy.lineup[i] = null;

                                }
                                else // otherwise just update Health and buffs
                                {
                                    EnemyLineupStatusHash.Add("H" + (i + 1), currentPlayerCreature.Health);
                                }

                                // TODO: create actions list

                            }
                            else // only player creature is present
                            {
                                context.Dungeon.enemy.health -= ((CatalogCardCreature)currentPlayerCreature.Card).attack;

                                // TODO: create actions list
                            }
                        }
                        else
                        {
                            if (currentEnemyCreature != null) // only enemy creature is present
                            {
                                context.Dungeon.player.health -= ((CatalogCardCreature)currentEnemyCreature.Card).attack;
                            }
                            // if both creatures are missing, nothing should be done
                        }

                    }

                    // update graveyards
                    playerLineupStatusHash.Add("PG", context.Dungeon.player.graveyard.Count);
                    EnemyLineupStatusHash.Add("EG", context.Dungeon.enemy.graveyard.Count);

                    // update heroes health
                    playerLineupStatusHash.Add("PH", context.Dungeon.player.health);
                    EnemyLineupStatusHash.Add("EH", context.Dungeon.enemy.health);

                    context.PluginHost.SetProperties(actorNr: 0, properties: EnemyLineupStatusHash, expected: null, broadcast: true); // actor=0 for Room properties
                    context.PluginHost.SetProperties(actorNr: 1, properties: playerLineupStatusHash, expected: null, broadcast: true);
                }

                // TODO: check if the game is ended and exit

                // add a point of mana every 3 seconds
                if (secondsFromStart % COFServerConsts.secondsPerMana == 0)
                {
                    if (context.Dungeon.enemy.mana <= COFServerConsts.maxMana)
                        context.PluginHost.SetProperties(actorNr: 0, properties: new Hashtable() { { "EM", context.Dungeon.enemy.mana++ } }, expected: null, broadcast: true); // actor=0 for Room properties
                    if (context.Dungeon.player.mana <= COFServerConsts.maxMana)
                        context.PluginHost.SetProperties(actorNr: 1, properties: new Hashtable() { { "PM", context.Dungeon.player.mana++ } }, expected: null, broadcast: true);
                }

                if (secondsFromStart % COFServerConsts.secondsPerCard == 0)
                {
                    // add card to players hand
                    if (context.Dungeon.player.hand.Count < COFServerConsts.numberOfCardsOnHand)
                    {
                        eventData.Add((byte)2, context.Dungeon.player.AddCardsOnHand().itemInstanceID);
                    }

                    if (context.Dungeon.enemy.hand.Count < COFServerConsts.numberOfCardsOnHand)
                    {
                        eventData.Add((byte)3, context.Dungeon.enemy.AddCardsOnHand().itemInstanceID);
                    }

                    // TODO: run AI moves

                }

                eventData.Add((byte)1, secondsFromStart);
                eventData.Add((byte)4, context.gameState);
                data.Add((byte)ParameterCode.Data, eventData);

                // finally raise the event to all players
                if (secondsFromStart % COFServerConsts.secondsPerTurn == 0)
                    context.PluginHost.BroadcastEvent(new int[] { 1 }, 0, (byte)gameEventType.endOfTurn, data, (byte)CacheOperations.DoNotCache);
                else
                    context.PluginHost.BroadcastEvent(new int[] { 1 }, 0, (byte)gameEventType.cycleUpdate, data, (byte)CacheOperations.DoNotCache);

            }

            context.gameState = gameState.finished;


        }

        public errorCodes ExecuteCard(int requester, PlayCardRequest requestedCard)
        {

            ICatalogItem card;
            COFPlayerProfile playerActor;
            LineupPosition battlefieldSlot = LineupPosition.topLane_1;

            // check from who this requests come and check the hand
            if (requester == context.Dungeon.player.ActorNr)
            {
                playerActor = context.Dungeon.player;
            }
            else // only alternative is AI
            {
                playerActor = context.Dungeon.enemy;
            }
            card = isCardInstanceIdInHand(playerActor.hand, requestedCard.instanceId);

            if (card == null)
                return errorCodes.cardNotFound;

            // check if target is acceptable for target's effect
            // switch (card.catalogItemType) TODO: ASAP!!!!!! change from Type to Enum
            if (card.catalogItemType == typeof(CatalogCardCreature))
            {
                // if creature, valid targets are only own battlefield empty slots
                if (requester == context.Dungeon.player.ActorNr)
                {
                    switch (requestedCard.target)
                    {
                        case battleTarget.position1_a:
                            battlefieldSlot = LineupPosition.topLane_1;
                            break;
                        case battleTarget.position2_a:
                            battlefieldSlot = LineupPosition.topLane_2;
                            break;
                        case battleTarget.position3_a:
                            battlefieldSlot = LineupPosition.midLane;
                            break;
                        case battleTarget.position4_a:
                            battlefieldSlot = LineupPosition.bottomLane_1;
                            break;
                        case battleTarget.position5_a:
                            battlefieldSlot = LineupPosition.bottomLane_2;
                            break;

                        default: // any other target is invalid
                            return errorCodes.invalidTarget;
                    }
                }
                else  // only alternative for now is computer AI
                {
                    switch (requestedCard.target)
                    {
                        case battleTarget.position1_b:
                            battlefieldSlot = LineupPosition.topLane_1;
                            break;
                        case battleTarget.position2_b:
                            battlefieldSlot = LineupPosition.topLane_2;
                            break;
                        case battleTarget.position3_b:
                            battlefieldSlot = LineupPosition.midLane;
                            break;
                        case battleTarget.position4_b:
                            battlefieldSlot = LineupPosition.bottomLane_1;
                            break;
                        case battleTarget.position5_b:
                            battlefieldSlot = LineupPosition.bottomLane_2;
                            break;

                        default: // any other target is invalid
                            return errorCodes.invalidTarget;
                    }
                }

                // check if the position is occupied
                if (playerActor.lineup[(int)battlefieldSlot] != null)
                    return errorCodes.invalidTarget;

            }

            // check mana
            if (playerActor.mana < ((CatalogBaseCard)card).manaCost)
                return errorCodes.notEnoughMana;

            // TODO: execute card
            if (card.catalogItemType == typeof(CatalogCardCreature))
            {
                PlayCreatureOnPosition(playerActor, (int)battlefieldSlot, (CatalogCardCreature)card);
            }
                
            // remove card from hand (no graveyard yet if Creature)
            if (card.catalogItemType == typeof(CatalogCardCreature))
            {
                playerActor.RemoveCardOnHand(card.itemInstanceID, false);
                
            }
            else
            {
                playerActor.RemoveCardOnHand(card.itemInstanceID);
            }

            return errorCodes.none;

        }

        private void PlayCreatureOnPosition(COFPlayerProfile Actor, int position, CatalogCardCreature card)
        {
            Hashtable creatureData = new Hashtable();

            Actor.lineup[position] = new PlayingCreature((short)position, card.itemInstanceID, (CatalogCardCreature)card, ((CatalogCardCreature)card).HP, null);

            creatureData.Add("I" + (position + 1), card.itemInstanceID);
            creatureData.Add("H" + (position + 1), card.HP);
            creatureData.Add("B" + (position + 1), 0);

            context.PluginHost.SetProperties(actorNr: Actor.ActorNr, properties: creatureData, expected: null, broadcast: true);

        }

        private static ICatalogItem isCardInstanceIdInHand(ICollection<ICatalogItem> hand, string instanceId)
        {
            foreach (ICatalogItem singleCard in hand)
            {
                if (singleCard.itemInstanceID == instanceId)
                    return singleCard;
            }

            return null;
        }

    }
}
