﻿using ExitGames.Logging;
using System.Collections.Generic;

namespace Photon.Hive.Plugin.WebHooks
{
    public class PluginFactory : IPluginFactory
    {

        private static readonly ILogger log = LogManager.GetCurrentClassLogger();

        public IGamePlugin Create(IPluginHost gameHost, string pluginName, Dictionary<string, string> config, out string errorMsg)
        {
            PluginBase plugin;
            plugin = new WebHooksPlugin();
            if (plugin.SetupInstance(gameHost, config, out errorMsg))
            {
                return plugin;
            }
            else
            {
                return null;
            }


        }
    }
}
