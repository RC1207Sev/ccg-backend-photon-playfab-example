﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using COF.Server;
using ExitGames.Logging;
using COF.Base;

namespace Photon.Hive.Plugin.WebHooks
{
    public class COFSinglePlayer : PluginBase
    {

        #region Fields

        protected bool SuccesfullLoaded;

        private bool hasErrorEvents;

        private bool isPersistentFlag;

        private COFPlayerProfile player = new COFPlayerProfile();

        private DungeonState Dungeon = new DungeonState();

        private static readonly ILogger log = LogManager.GetCurrentClassLogger();

        #endregion

        #region Public Properties

        public override string Name
        {
            get
            {
                return "SP";
            }
        }

        #endregion

        public COFSinglePlayer()
        {
            this.UseStrictMode = true;
        }

        #region Public Methods and Operators

        public override void OnCloseGame(ICloseGameCallInfo info)
        {
            // Discard all game logics and data

                info.Continue();
        }

        async public override void OnCreateGame(ICreateGameCallInfo info)
        {

            // Initialize game

            try
            {
                // PlayfabId of player
                player.playfabId = this.PluginHost.GameProperties["PI"].ToString();

                // Dungeon name
                Dungeon.name = this.PluginHost.GameProperties["DN"].ToString();

                log.Info("Player " + player.playfabId + " requests to play on dungeon " + Dungeon.name);

                player.ActorNr = info.UserId;

                // Get player Inventory

                await BackendManager.Instance.GetInventory(player);

                // Get player's Deck

                await BackendManager.Instance.GetDeck(player);

                // Get dungeon data

                // Set up Lineups

                // Start game mechanics thread (with AI)
                info.Continue();

            }
            catch (Exception e)
            {
                log.Error("Unable to initialize game " + info.Nickname + ". Error: " + e.Message);
                info.Fail(e.Message);
            }


            if (!info.IsJoin)
            {
               
            }
            else
            {
                //info.Continue();
            }

            
        }

        public override void OnJoin(IJoinGameCallInfo info)
        {
            // no sync OnJoin support for now - since we already changed the room state when we get here.
            // to allow a clean sync (cancelable OnJoin) we need to 
            // a. run all the pre-checks
            // b. call OnJoin with the expected new actornr
            // c. on continue - add peer to room 

            //if (!string.IsNullOrEmpty(this.gameJoinUrl))
            //{
            //    // remove for callAsync=false
            //    info.Continue();

            //    this.PostJsonRequest(
            //        this.gameJoinUrl,
            //        new WebhooksRequest
            //        {
            //            Type = "Join",
            //            GameId = this.PluginHost.GameId,
            //            AppId = this.AppId,
            //            AppVersion = this.AppVersion,
            //            Region = this.Region,
            //            UserId = info.UserId,
            //            Nickname = info.Nickname,
            //            ActorNr = info.ActorNr,
            //            AuthCookie = info.AuthCookie,
            //        },
            //        this.LogIfFailedCallback,
            //        info,
            //        callAsync: true);
            //}
            //else
            //{
            //    info.Continue();
            //}
        }

        public override void OnLeave(ILeaveGameCallInfo info)
        {
            base.OnLeave(info);

            //var url = this.gameLeaveUrl;
            //if (!string.IsNullOrEmpty(url)) // && info.ActorNr != -1 we don't restrict this anymore as in forwardplugin to see all leaves
            //{
            //    this.PostJsonRequest(
            //        url,
            //        new WebhooksRequest
            //        {
            //            Type = LeaveReason.ToString(info.Reason),
            //            GameId = this.PluginHost.GameId,
            //            AppId = this.AppId,
            //            AppVersion = this.AppVersion,
            //            Region = this.Region,
            //            UserId = info.UserId,
            //            Nickname = info.Nickname,
            //            ActorNr = info.ActorNr,
            //            IsInactive = info.IsInactive,
            //            Reason = info.Reason.ToString(),
            //            AuthCookie = WebFlags.ShouldSendAuthCookie(info.Request.WebFlags) ? info.AuthCookie : null,
            //        },
            //        this.LogIfFailedCallback,
            //        callAsync: true);
            //}
        }

        public override void OnRaiseEvent(IRaiseEventCallInfo info)
        {
            base.OnRaiseEvent(info);

            //var raiseEventRequest = info.Request;
            //var url = this.gameEventUrl;

            //if (raiseEventRequest.HttpForward && !string.IsNullOrEmpty(url))
            //{
            //    var state = WebFlags.ShouldSendState(info.Request.WebFlags) ? this.GetGameState() : null;

            //    this.PostJsonRequest(
            //        url,
            //        new WebhooksRequest
            //        {
            //            Type = "Event",
            //            GameId = this.PluginHost.GameId,
            //            AppId = this.AppId,
            //            AppVersion = this.AppVersion,
            //            Region = this.Region,
            //            UserId = info.UserId,
            //            Nickname = info.Nickname,
            //            ActorNr = info.ActorNr,
            //            Data = raiseEventRequest.Data,
            //            State = state,
            //            AuthCookie = WebFlags.ShouldSendAuthCookie(info.Request.WebFlags) ? info.AuthCookie : null,
            //            EvCode = raiseEventRequest.EvCode,
            //        },
            //        this.LogIfFailedCallback,
            //        null,
            //        callAsync: !WebFlags.ShouldSendSync(info.Request.WebFlags));
            //}
        }

        private SerializableGameState GetGameState()
        {
            if (this.IsPersistent)
            {
                try
                {
                    return this.PluginHost.GetSerializableGameState();
                }
                catch (Exception ex)
                {
                    this.PluginHost.LogFatal(ex.Message);
                    throw;
                }
            }
            return null;
        }

        public override void OnSetProperties(ISetPropertiesCallInfo info)
        {
            base.OnSetProperties(info);

            //var setPropertiesRequest = info.Request;
            //var url = this.gamePropertiesUrl;

            //if (setPropertiesRequest.HttpForward && !string.IsNullOrEmpty(url))
            //{
            //    var state = WebFlags.ShouldSendState(info.Request.WebFlags) ? this.GetGameState() : null;

            //    this.PostJsonRequest(
            //        url,
            //        new WebhooksRequest
            //        {
            //            Type = info.Request.ActorNumber == 0 ? "Game" : "Actor",
            //            TargetActor = info.Request.ActorNumber == 0 ? null : (int?)info.Request.ActorNumber,
            //            GameId = this.PluginHost.GameId,
            //            AppId = this.AppId,
            //            AppVersion = this.AppVersion,
            //            Region = this.Region,
            //            UserId = info.UserId,
            //            Nickname = info.Nickname,
            //            ActorNr = info.ActorNr,
            //            Properties = setPropertiesRequest.Properties,
            //            State = state,
            //            AuthCookie = WebFlags.ShouldSendAuthCookie(info.Request.WebFlags) ? info.AuthCookie : null,
            //        },
            //        this.LogIfFailedCallback,
            //        null,
            //        callAsync: !WebFlags.ShouldSendSync(info.Request.WebFlags));
            //}
        }

        public override bool SetupInstance(IPluginHost host, Dictionary<string, string> config, out string errorMsg)
        {
            if (!base.SetupInstance(host, config, out errorMsg))
            {
                return false;
            }

            if (BackendManager.Instance.status != initState.initialized)
            {
                errorMsg = "Backend not initialized";
                return false;
            }

            //if (config != null && config.ContainsKey(BaseUrlKey))
            //{
            //    //                this.pluginConfig = config;

            //    this.baseUrl = this.GetKeyValue(config, BaseUrlKey);
            //    if (!string.IsNullOrEmpty(this.baseUrl))
            //    {
            //        this.isPersistentFlag = this.GetBoolKeyValue(config, GameIsPersistentKey);
            //        this.hasErrorEvents = this.GetBoolKeyValue(config, GameHasErrorEventsKey);

            //        this.baseUrl = this.baseUrl.Trim();

            //        this.gameCreatedUrl = this.GetUrl(config, GameCreateKey);
            //        this.gameClosedUrl = this.GetUrl(config, GameCloseKey);
            //        this.gameJoinUrl = this.GetUrl(config, GameJoinKey);
            //        this.gameLeaveUrl = this.GetUrl(config, GameLeaveKey);
            //        this.gameLoadUrl = this.GetUrl(config, GameLoadKey);
            //        this.gamePropertiesUrl = this.GetUrl(config, GamePropertiesKey);
            //        this.gameEventUrl = this.GetUrl(config, GameEventKey);
            //        var headers = this.GetKeyValue(config, CustomHttpHeadersKey);
            //        if (!string.IsNullOrEmpty(headers))
            //        {
            //            this.customHttpHeaders = JsonConvert.DeserializeObject<Dictionary<string, string>>(headers);
            //        }

            //        host.LogDebug(this.gameCreatedUrl);
            //        host.LogDebug(this.gameClosedUrl);
            //        host.LogDebug(this.gameEventUrl);

            //        //                    this.PluginHost.LogDebug("SetupInstance config:" + config.SerializeToString());
            //        return true;
            //    }
            //}

            //errorMsg = string.Format("Non null 'config' containing key '{0}' with non empty string value expected.", BaseUrlKey);
            return true;
        }

        #endregion

    }
}
