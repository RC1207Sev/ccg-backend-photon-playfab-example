﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using COF.Base;
using PlayFab;
using COF.Playfab;
using PlayFab.ServerModels;
using ExitGames.Logging;

namespace COF.Server
{
    public enum loginMethod
    {
        zerofriction = 1,
        fb = 2,
        gp = 3,
        ios = 4
    }

    public enum initState
    {
        notInitialized = 0,
        initializing = 1,
        initialized = 2
    }

    //!  Class to handle all backend related operations. 
    /*!
      This class has a collection of operations to communicate with the backend system (both database and realtime) 
      and will also keep track of all server side data regarding the specific game session.
    */
    public class BackendManager
    {

        private static BackendManager instance;

        public BackendManager() { }

        public static BackendManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new BackendManager();
                }
                return instance;
            }
        }

        public string SessionTicket;                                                /*!< Session ticket with Playfab. To keep track about connection with the server */
        public string PlayFabId;                                                    /*!< Player's id on Playfab server */

        private static readonly ILogger log = LogManager.GetCurrentClassLogger();

        public initState status = initState.notInitialized;

        public List<ICatalogItem> catalog = new List<ICatalogItem>();               /*!< Collection of game elements downloaded each login from the server */

        public List<ICatalogItem> inventory = new List<ICatalogItem>();             /*!< Player's inventory (deprecated). TODO: MUST be removed from this class */

        public List<ICatalogItem> deck = new List<ICatalogItem>();                  /*!< Player's deck (deprecated). TODO: MUST be removed from this class */

        // Virtual Currencies
        public int gold;                                /*!< Amount of gold coins the player has */
        public int soulDust;                            /*!< Amount of Soul Dust the player has */
        public int energy;                              /*!< Amount of energy remaining (will be removed) */
        public DateTime nextEnergyRechargeTime;         /*!< Time of the next energy increment by the server */


        private Action<BackendInitResult> _callback = null;             /*!< this variable will store the callback to execute once the backend has been initialized */
        private bool isNewlyCreated = false;                            /*!< true if the login attemps come from a new profile (server will create a new profile automatically) */

        private String deckString = "[]";                               /*!< initialize the empty deck string to send to Playfab */

        //! Backend Initialization.
        /*!
          This method will initialize Playfab connection from this server. It is mandatory to call this method before using this class.
            \param callback method to callback on server response. The method will require a BackendInitResult input parameter
            \return nothing (will call callback with BackendInitResult result)
        */
        public async System.Threading.Tasks.Task<bool> Init(Action<BackendInitResult> callback)
        {
            status = initState.initializing;

            PlayFabSettings.TitleId = "";
            PlayFabSettings.DeveloperSecretKey = "";

            PF_Bridge.OnPlayFabCallbackError += (details, method, style) =>
            {
                log.Error("Error occurred on " + method.ToString() + " with detail:" + details);
            };

            _callback = callback;

            await ConnectToPlayFab();

            return true;

        }

        async System.Threading.Tasks.Task<bool> ConnectToPlayFab()
        {

            float tempVer;
            int tempInt;
            string tempStr;
            bool tempBool = true;
            String[] itemClassLevels;

            Dictionary<string, string> custData;
            

            GetCatalogItemsRequest catalogRequest = new GetCatalogItemsRequest()
            {
                CatalogVersion = null
            };
            PlayFabResult<GetCatalogItemsResult> catalogResult = await PlayFabServerAPI.GetCatalogItemsAsync(catalogRequest);

            // meanwhile, create ICatalogItem objects
            catalog.Clear();

            foreach (CatalogItem catItem in catalogResult.Result.Catalog)
            {
                itemClassLevels = catItem.ItemClass.Split(':');

                switch (itemClassLevels[0])
                {
                    case "card":

                        switch (itemClassLevels[1]) // which kind of card we have
                        {
                            case "creature":
                                CatalogCardCreature cardCreature = new CatalogCardCreature();   //  (CatalogCardCreature) temp;

                                cardCreature = (CatalogCardCreature)InitItem(cardCreature, catItem);

                                cardCreature.catalogItemType = typeof(CatalogCardCreature);
                                
                                custData = PlayFab.Json.JsonWrapper.DeserializeObject<Dictionary<string, string>>(catItem.CustomData);

                                if (float.TryParse(custData["Version"], out tempVer))
                                    cardCreature.itemVersion = tempVer;

                                if (int.TryParse(custData["Soul Dust Disenchant Cost"], out tempInt))
                                    cardCreature.DisenchantCost = tempInt;

                                cardCreature.soundEffect = custData["Sound Effects"];

                                cardCreature.animEffect = custData["Animation Effects"];

                                if (int.TryParse(custData["Soul Dust Purchase Cost"], out tempInt))
                                    cardCreature.PurchaseCost = tempInt;

                                if ((custData.TryGetValue("disenchantable", out tempStr)))
                                    bool.TryParse(custData["disenchantable"], out tempBool);
                                else
                                    tempBool = true;

                                ((CatalogBaseCard)cardCreature).disenchantable = tempBool;

                                cardCreature.Rarity = custData["Rarity"];

                                if (int.TryParse(custData["Mana Cost"], out tempInt))
                                    cardCreature.manaCost = tempInt;

                                // cardCreature.Element = custData["Element"];  // obsolete

                                if (int.TryParse(custData["HP"], out tempInt))
                                    cardCreature.HP = tempInt;

                                // cardCreature.dungeon = custData["Dungeon"];  // obsolete

                                cardCreature.skill = custData["Skill"];

                                if (int.TryParse(custData["ATK"], out tempInt))
                                    cardCreature.attack = tempInt;

                                cardCreature.classCreature = custData["Class"];

                                // card skill data
                                cardCreature.cardSkill = new COFSkill(custData["SkillName"], custData["SkillExecTime"], custData["SkillParam"], custData["SkillTarget"]);

                                catalog.Add(cardCreature);

                                break;

                            case "spell":
                                CatalogCardSpell cardSpell = new CatalogCardSpell(); // (CatalogCardSpell) temp;

                                cardSpell = (CatalogCardSpell)InitItem(cardSpell, catItem);

                                cardSpell.catalogItemType = typeof(CatalogCardSpell);

                                custData = PlayFab.Json.JsonWrapper.DeserializeObject<Dictionary<string, string>>(catItem.CustomData);

                                if (float.TryParse(custData["Version"], out tempVer))
                                    cardSpell.itemVersion = tempVer;

                                if (int.TryParse(custData["Soul Dust Disenchant Cost"], out tempInt))
                                    cardSpell.DisenchantCost = tempInt;

                                if ((custData.TryGetValue("disenchantable", out tempStr)))
                                    bool.TryParse(custData["disenchantable"], out tempBool);
                                else
                                    tempBool = true;

                                ((CatalogBaseCard)cardSpell).disenchantable = tempBool;

                                // TODO: once we will have the sound and animation effects, decomment next 2 lines

                                //cardSpell.soundEffect = custData["Sound Effects"];

                                //cardSpell.animEffect = custData["Animation Effects"];

                                if (int.TryParse(custData["Soul Dust Purchase Cost"], out tempInt))
                                    cardSpell.PurchaseCost = tempInt;

                                cardSpell.Rarity = custData["Rarity"];

                                if (int.TryParse(custData["Mana Cost"], out tempInt))
                                    cardSpell.manaCost = tempInt;

                                cardSpell.info = custData["Info"];

                                cardSpell.Effect = custData["Effect"];

                                // card skill data
                                cardSpell.cardSkill = new COFSkill(custData["SkillName"], custData["SkillExecTime"], custData["SkillParam"], custData["SkillTarget"]);

                                catalog.Add(cardSpell);

                                break;

                            case "weapon":

                                CatalogCardWeapon newWeapon = new CatalogCardWeapon();   //  (CatalogCardCreature) temp;

                                newWeapon = (CatalogCardWeapon)InitItem(newWeapon, catItem);

                                newWeapon.catalogItemType = typeof(CatalogCardWeapon);

                                custData = PlayFab.Json.JsonWrapper.DeserializeObject<Dictionary<string, string>>(catItem.CustomData);

                                if (float.TryParse(custData["Version"], out tempVer))
                                    newWeapon.itemVersion = tempVer;

                                if (int.TryParse(custData["Soul Dust Disenchant Cost"], out tempInt))
                                    newWeapon.DisenchantCost = tempInt;

                                //newWeapon.soundEffect = custData["Sound Effects"];

                                //newWeapon.animEffect = custData["Animation Effects"];

                                if (int.TryParse(custData["Soul Dust Purchase Cost"], out tempInt))
                                    newWeapon.PurchaseCost = tempInt;

                                newWeapon.Rarity = custData["Rarity"];

                                if ((custData.TryGetValue("disenchantable", out tempStr)))
                                    bool.TryParse(custData["disenchantable"], out tempBool);
                                else
                                    tempBool = true;

                                ((CatalogBaseCard)newWeapon).disenchantable = tempBool;

                                if (int.TryParse(custData["Mana Cost"], out tempInt))
                                    newWeapon.manaCost = tempInt;

                                if (int.TryParse(custData["HP"], out tempInt))
                                    newWeapon.HP = tempInt;

                                newWeapon.skill = custData["Skill"];

                                if (int.TryParse(custData["ATK"], out tempInt))
                                    newWeapon.attack = tempInt;

                                // card skill data
                                newWeapon.cardSkill = new COFSkill(custData["SkillName"], custData["SkillExecTime"], custData["SkillParam"], custData["SkillTarget"]);

                                catalog.Add(newWeapon);
                                break;
                            default:
                                break;
                        }


                        break;

                    case "bundle":
                        CatalogBundle newBundle = new CatalogBundle();

                        newBundle = (CatalogBundle)InitItem(newBundle, catItem);

                        newBundle.catalogItemType = typeof(CatalogBundle);

                        newBundle.RandomValues = catItem.Bundle.BundledResultTables;

                        newBundle.VirtualCurrencies = catItem.Bundle.BundledVirtualCurrencies;

                        foreach (string bundleItem in catItem.Bundle.BundledItems)
                        {
                            if (!newBundle.bundleItems.ContainsKey(bundleItem))
                                newBundle.bundleItems.Add(bundleItem, GetCatalogItemById(bundleItem));
                        }

                        catalog.Add(newBundle);

                        break;
                    default:

                        CatalogGenericItem tempGenericItem = new CatalogGenericItem();

                        tempGenericItem = (CatalogGenericItem)InitItem(tempGenericItem, catItem);

                        catalog.Add(tempGenericItem);
                        log.Debug("Error on catalog items: " + catItem.DisplayName + " class not found: " + catItem.ItemClass);
                        break;
                }
            }

            if (_callback != null)
                _callback(new BackendInitResult(true,null,false));

            status = initState.initialized;

            return true;

        }

        //! Get Player's inventory.
        /*!
          Will get player's inventory from Playfab.
            \param player's profile. Used to get player's id and store result's data
            \return nothing (will save information on passed player's profile)
        */
        public async System.Threading.Tasks.Task<bool> GetInventory(COFPlayerProfile player)
        {

            ICatalogItem tempItem;
            bool tempBool;
            string tempStr;
            String[] itemClassLevels;

            GetUserInventoryRequest inventoryRequest = new GetUserInventoryRequest();
            inventoryRequest.PlayFabId = player.playfabId;

            PlayFabResult<GetUserInventoryResult> inventoryResult = await PlayFabServerAPI.GetUserInventoryAsync(inventoryRequest);

            log.Info("Player " + player.playfabId + " Inventory items: " + inventoryResult.Result.Inventory.Count);

            player.inventory.Clear();

            foreach (ItemInstance invItem in inventoryResult.Result.Inventory)
            {

                tempItem = GetCatalogItemClone(invItem.ItemId);

                if(tempItem != null)
                {
                    tempItem.itemInstanceID = invItem.ItemInstanceId;

                    itemClassLevels = invItem.ItemClass.Split(':');

                    switch (itemClassLevels[0])
                    {
                        case "card":

                            if ((invItem.CustomData != null) && (invItem.CustomData.TryGetValue("disenchantable", out tempStr)))
                                bool.TryParse(invItem.CustomData["disenchantable"], out tempBool);
                            else
                                tempBool = true;

                            ((CatalogBaseCard)tempItem).disenchantable = tempBool;

                            break;

                        default:
                            break;
                    }

                    player.inventory.Add(tempItem);
                    log.Info("name: " + tempItem.displayName + " id: " + tempItem.itemInstanceID);
                }
                else
                {
                    log.Error("Error in Inventory: item " + invItem.ItemId + " not found");
                }


            }

            return true;
        }

        //! Request dungeon reward to playfab.
        /*!
          Once a player wins a dungeon battle, the server will require dungeon's specific reward 
          to be assigned to player's inventory.
          The reward is a closed Container which has to be open from client side 
          with playfab's API.
            \param player player's profile. Used to get player playfab's id
            \param dungeon current dungeon. Used to get dungeon playfab's id and specific reward
            \return string of reward's playfab's ID (not the InstanceID)
        */
        public async System.Threading.Tasks.Task<string> AddReward(COFPlayerProfile player, DungeonState dungeon)
        {

            GrantItemsToUserRequest rewardRequest = new GrantItemsToUserRequest();

            rewardRequest.PlayFabId = player.playfabId;
            // TODO: add dungeon info for the reward
            rewardRequest.ItemIds = new List<string>(new String[] { "testdungeon_reward" });

            PlayFabResult<GrantItemsToUserResult> rewardResult = await PlayFabServerAPI.GrantItemsToUserAsync(rewardRequest);

            if (rewardResult.Result.ItemGrantResults.Count > 0)
                return rewardResult.Result.ItemGrantResults[0].ItemId;
            else
                return null;

        }

        private void AfterGetCatalog()
        {

            ICatalogItem tempItem;
            Dictionary<string, string> custData;
            float tempVer;
            int tempInt;
            string tempStr;
            bool tempBool = true;
            String[] itemClassLevels;

            log.Debug("Catalog loaded. Items found: " + catalog.Count);

            // Inventory

            log.Debug("Inventory items: " + inventory.Count);

            foreach (ItemInstance invItem in inventory)
            {

                tempItem = GetCatalogItemById(invItem.ItemId);

                tempItem.itemInstanceID = invItem.ItemInstanceId;

                itemClassLevels = invItem.ItemClass.Split(':');

                switch (itemClassLevels[0])
                {
                    case "card":

                        if ((invItem.CustomData != null) && (invItem.CustomData.TryGetValue("disenchantable", out tempStr)))
                            bool.TryParse(invItem.CustomData["disenchantable"], out tempBool);
                        else
                            tempBool = true;

                        ((CatalogBaseCard)tempItem).disenchantable = tempBool;

                        break;

                    default:
                        break;
                }

                inventory.Add(tempItem);

            }


            // Creation of Deck

            String[] deckCards = PlayFab.Json.JsonWrapper.DeserializeObject<string[]>(deckString);
            ICatalogItem tempDeckItem;

            deck.Clear();

            foreach (string deckCardInventoryId in deckCards)
            {
                tempDeckItem = GetInventoryItemByInstanceId(deckCardInventoryId, deck);

                if (tempDeckItem != null)
                {
                    deck.Add(tempDeckItem);
                }
                else
                {
                    log.Debug("Deck creation Error: ItemInstanceId " + deckCardInventoryId + " not found in Inventory");
                }

            }

            // End of initalization
            // Signal the caller that we have finished intialization with the passes callback
            if (_callback != null)
            {
                _callback(new BackendInitResult(true, null, isNewlyCreated));
            }

        }

        private ICatalogItem InitItem(ICatalogItem item, CatalogItem catItem)
        {

            item.displayName = catItem.DisplayName;
            item.itemID = catItem.ItemId;
            item.tags = catItem.Tags;
            item.itemImageURL = catItem.ItemImageUrl;
            item.description = catItem.Description;
            item.isStackable = catItem.IsStackable;

            return item;

        }

        private void AfterGetInventory()
        {
            // End of initalization
            // Signal the caller that we have finished intialization with the passes callback
            log.Debug("Get Inventory Executed Succesfully. Total items: " + inventory.Count);
            if (_callback != null)
            {
                _callback(new BackendInitResult(true, null, isNewlyCreated));
            }
        }

        //! Get Player's deck.
        /*!
          Will get player's deck from Playfab.
            \param player player's profile. Used to get player's id and store result's data
            \return nothing (will save information on passed player's profile)
        */
        public async System.Threading.Tasks.Task<bool> GetDeck(COFPlayerProfile player)
        {

            // get ReadOnly user data for Deck1
            GetUserDataRequest userDataRequest = new GetUserDataRequest();
            userDataRequest.PlayFabId = player.playfabId;
            PlayFabResult<GetUserDataResult> userDataResult = await PlayFabServerAPI.GetUserReadOnlyDataAsync(userDataRequest);

            // Creation of Deck

            String[] deckCards = PlayFab.Json.JsonWrapper.DeserializeObject<string[]>(userDataResult.Result.Data["Deck1"].Value);
            ICatalogItem tempDeckItem;

            log.Info("Player " + player.playfabId + " Deck items: " + deckCards.Length);

            player.deck.Clear();

            foreach (string deckCardInventoryId in deckCards)
            {
                tempDeckItem = GetInventoryItemByInstanceId(deckCardInventoryId, player.inventory);

                if (tempDeckItem != null)
                {
                    log.Info(tempDeckItem.displayName);
                    player.deck.Add(tempDeckItem);
                }
                else
                {
                    log.Error("Deck creation Error: ItemInstanceId " + deckCardInventoryId + " not found in Inventory");
                    return false;
                }

            }

            return true;
        }

        private static ICatalogItem GetCatalogItemClone(string id)
        {
            foreach (ICatalogItem singleItem in BackendManager.Instance.catalog)
            {
                if (singleItem.itemID == id)
                    return (ICatalogItem)COFUtils.CloneObject(singleItem, singleItem.catalogItemType);
            }

            return null;
        }

        public static ICatalogItem GetCatalogItemById(string id)
        {
            foreach (ICatalogItem singleItem in BackendManager.Instance.catalog)
            {
                if (singleItem.itemID == id)
                    return singleItem;
            }

            return null;
        }

        private static ICatalogItem GetInventoryItemByInstanceId(string instanceId, List<ICatalogItem> pDeck)
        {
            foreach (ICatalogItem singleItem in pDeck)
            {
                if (singleItem.itemInstanceID == instanceId)
                    return singleItem;
            }

            return null;
        }


    }

}
