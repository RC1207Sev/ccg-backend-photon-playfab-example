﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COFBackendIntegration.BackendManager
{
    //!  Server Constants
    /*!
      This class contains all the constant used for server game logic.
    */
    public static class COFServerConsts
    {

        // in-game mechanics
        public const short secondsPerMana = 2;                          /*!< Number of seconds for the regeneration of 1 point of mana */
        public const short secondsPerCard = 10;                         /*!< Number of seconds to draw a new card from the deck */
        public const short secondsPerTurn = 20;                         /*!< Duration of one game turn in seconds */

        public const short maxGameLenghtInSeconds = 300;                /*!< Max duration of a game in seconds */
        public const short maxMana = 10;                                /*!< Maximum amount of mana a player can have (player can have more than 10 points, but the regeneration system will just stop */
        public const int maxHealth = 10;                              /*!< Player's starting health points */

        public const short usedCardsBufferSize = 0;                     /*!< Size of the used cards buffer (deprecated). Used to delay the replayability of cards once used */

        // plug-in
        public const short numberOfCardsOnHand = 5;                     /*!< Maximum amount of cards a player can have in his hand */
        public const short maxPlayerPreparationTimeInSeconds = 10;      /*!< Maximum duration in seconds of the preparation phase for players before the battle begins. In this phase, players can redraw some of the cards in their hands */
        public const short secondsPerAIDecision = 3;
    }
}
