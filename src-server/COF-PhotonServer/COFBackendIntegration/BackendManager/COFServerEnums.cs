﻿
namespace COFBackendIntegration.BackendManager
{
    //!  Collection of Enums used for networking. 

    public static class COFServerEnums
    {
        /** List of Playfab logins methods
         *  These values are used to inform Playfab backend on which login method the player has adopted.
         */
        public enum loginMethod
        {
            zerofriction = 1,       /*!< Playfab's built in login with no user or pass (device ID) */
            fb = 2,                 /*!< Facebook */
            gp = 3,                 /*!< Google Play*/
            ios = 4                 /*!< Apple login*/
        }

        public enum cardSkill
        {
            none,
            dealDamage,
            increaseAttack,
            challengingShout,
            drawCards,
            restoreHealth,
            killWoundedCreature,
            addSpell,
            decreaseCostNextWeapon,
            decreaseCostSelectedCard,
            synergy,
            SwitchCreaturePosition,
            restoreMana,
            silence,
            ResurrectCreature,
            addStatus

        }

        public enum cardSkillExecutionTime
        {
            none,
            impact,
            finalAct,
            always,
            turns
        }

        public enum cardSkillTarget
        {
            none,
            caster,
            enemy_hero,
            self,
            friendly,
            enemy_creature,
            all_friendly,
            all_enemy,
            all_heroes,
            all_creatures,
            all,
            random,
            random_enemy,
            random_friendly,
            lane
        }

        public enum creatureStatus : byte
        {
            none,
            silenced,
            stunned,
            challenging,
            immune
        }

        /** List of game events
         *  These events type are both sent and received to/from the real-time server.
         */
        public enum gameEventType : byte
        {
            playerReady = 110,
            serverReady = 111,
            serverWaitingPlayers = 112,
            cycleUpdate = 113,
            requestUseCard = 114,
            cardEffect = 115,
            requestRedraw = 116,
            ExecutedRedraw = 118,
            ExecutedCard = 119,
            endOfTurn = 120,
            gameStateChanged = 121,
            endOfGame = 122
        }

        /** List of player states
         *  These are the possible states a player can have during a real-time game.
         */
        public enum playerConnectionStatus : byte
        {
            connected,
            ready,
            waiting,
            playing,
            disconnected
        }

        /** List of game states
         *  These are the possible states of a real-time game.
         */
        public enum gameState : byte
        {
            initializing,
            waitingForPlayers,
            paused,
            running,
            finished,
            closed
        }

        /** List of event data type
         *  These values are used as keys for the Hashtable returned to/sent from the server.
         */
        public enum eventDataType : byte
        {
            gameTime,
            RedrawCards,
            PlayedCard,
            ExecutionResult,
            errorCode,
            photonActions,
            gameState,
            winner,
            reward
        }

        /** List of targets on the battlefield
         *  These values are used to inform the server on where to use a card
         */
        public enum battleTarget : byte
        {
            player,
            position1_a,
            position2_a,
            position3_a,
            position4_a,
            position5_a,
            position1_b,
            position2_b,
            position3_b,
            position4_b,
            position5_b,
            enemy,
            none
        }

        /** List of positions in the battlefield
         *  These values are used to store the battlefield position of your own creatures.
         */
        public enum LineupPosition : int
        {
            topLane_1,
            topLane_2,
            midLane,
            bottomLane_1,
            bottomLane_2
        }

        /** List of possible error codes
         *  These values are used from the server to inform the player of potential errors on card execution
         */
        public enum errorCodes : int
        {
            notEnoughMana,
            invalidTarget,
            cardNotFound,
            executionFailed,
            none
        }

        /** List of available photon actions
         *  Those values are used to identify the specific kinds of photon actions sent through the network
         */
        public enum photonActionType : byte
        {
            creatureDamage,
            creatureDeath,
            creatureAdded,
            addedCardtoHand,
            addedEquipment,
            playerDamage,
            playerKilled,
            healing,
            changeMana,
            addedStatus
        }

    }
}
