﻿using COF.Base;
using System;
using System.Collections.Generic;
using COFBackendIntegration.BackendManager;

namespace COF.Server
{
    //!  Game player profile. 
    /*!
      This class will keep all player data during the game.
    */
    public class COFPlayerProfile
    {

        public List<ICatalogItem> inventory = new List<ICatalogItem>();     /*!< Player inventory (Playfab) */

        public List<ICatalogItem> deck = new List<ICatalogItem>();          /*!< Player deck */

        public List<ICatalogItem> hand = new List<ICatalogItem>();          /*!< Player hand */

        public List<ICatalogItem> usedCards = new List<ICatalogItem>();     /*!< Buffer of recently used cards */

        public List<ICatalogItem> graveyard = new List<ICatalogItem>();     /*!< Player graveyard. All the used cards will go here */

        public PlayingCreature[] lineup = new PlayingCreature[5];           /*!< Player's lineup. All creatures that are playing on the battleground (null if empty) */

        public int mana = 0;                                                /*!< Player's mana (max value COFServerConsts.maxMana) */

        public int health = 30;                                             /*!< Player's health (max value COFServerConsts.maxHealth) */

        public string playfabId;                                            /*!< Player's playfab Id */

        public bool redrawedCards = false;                                  /*!< If player has already redrawn his cards (only once per game is allowed) */

        public int ActorNr;                                                 /*!< Player's photon's room actor number */

        public COFServerEnums.playerConnectionStatus status = COFServerEnums.playerConnectionStatus.connected;  /*!< Game status (see COFServerEnums.playerConnectionStatus for more info) */

        //! Adds a given number of cards on hand.
        /*!
          Adds a card on the hand, removing from the deck. It will return the selected card. 
            \param numberOfCards number of cards to add (the method will add only until the number of cards on hand is less than COFServerConsts.numberOfCardsOnHand)
            \return the ICatalogItem of the added card (WARNING: since it can be used to add more than 1 card, it's returned card is just the last card added. The return value should not be considered for multiple adds)
        */
        public ICatalogItem AddCardsOnHand(short numberOfCards = 1)
        {
            int randomCard;
            ICatalogItem addedCard = null;
            Random randomizer = new Random();

            // Check if we still have available cards in deck
            if (deck.Count < numberOfCards)
            {
                // if not enough cards are available, ignore call and return null
                return null;
            }

            //hand.Clear();
            for (int i = 0; i < numberOfCards; i++)
            {
                randomCard = randomizer.Next(deck.Count);
                hand.Add(deck[randomCard]);
                addedCard = deck[randomCard];
                deck.RemoveAt(randomCard);
            }

            return addedCard;
        }


        //! Removes a given number of cards on hand.
        /*!
          This method will add the card to the graveyard once removed.
          User can skip this step but be advised, by doing so you may lose the card during game
          since it's not anymore in deck or hand.
            \param cardInstanceId instanceId string of the card to remove
            \param addToGraveyard to specify if you need to add the card on the graveyard once removed (if removed during redrawing phase or adding a creature in game, it's not necessary to add on graveyard)
            \return the ICatalogItem of the removed card. Returns null if the card is not found
        */
        public ICatalogItem RemoveCardOnHand(string cardInstanceId, bool addToGraveyard = true)
        {
            foreach (ICatalogItem singleCard in hand)
            {
                if (singleCard.itemInstanceID == cardInstanceId)
                {
                    if (addToGraveyard)
                        graveyard.Add(singleCard);
                    hand.Remove(singleCard);
                    return singleCard;
                }
                    
            }

            return null;

        }

        // TODO: comments!!!
        public int Damage(int amount)
        {
            // TODO: start considering buffs
            health -= amount;

            return amount;

        }

        // TODO: comments!!!
        public int Heal(int amount)
        {
            // TODO: start considering buffs
            health += amount;

            return amount;

        }

        // TODO: comments!!!
        public int AddMana(int manaToAdd)
        {
            mana += manaToAdd;

            return mana;
        }
    }
}