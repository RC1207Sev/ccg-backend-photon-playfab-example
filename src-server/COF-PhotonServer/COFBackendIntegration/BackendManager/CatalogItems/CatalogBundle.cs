﻿using System.Collections;
using COF.Base;
using System.Collections.Generic;

namespace COF.Server
{
    public class CatalogBundle : CatalogGenericItem
    {
        public Dictionary<string, ICatalogItem> bundleItems = new Dictionary<string, ICatalogItem>();

        public List<string> RandomValues { get; internal set; }
        public Dictionary<string, uint> VirtualCurrencies { get; internal set; }
    }
}
