﻿using System.Collections;


namespace COF.Server
{
    public class CatalogCardWeapon : CatalogBaseCard
    {
        public int HP { get; internal set; }
        public string skill { get; internal set; }
        public int attack { get; internal set; }
    }
}
