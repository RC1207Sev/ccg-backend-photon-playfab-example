﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace COF.Server
{
    public class CatalogCardCreature : CatalogBaseCard
    {

        public string Element { get; internal set; }
        public int HP { get; internal set; }
        public string dungeon { get; internal set; }
        public string skill { get; internal set; }
        public int attack { get; internal set; }
        public string classCreature { get; internal set; }

    }
}
