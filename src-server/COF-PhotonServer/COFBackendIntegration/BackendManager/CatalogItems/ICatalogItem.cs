﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace COF.Base
{
    public interface ICatalogItem
    {
        Type catalogItemType { get; set; }
        string itemID { get; set; }
        string itemInstanceID { get; set; }
        string displayName { get; set; }
        string itemClass { get; set; }
        List<string> tags { get; set; }
        string itemImageURL { get; set; }
        string description { get; set; }
        bool isStackable { get; set; }
    }
}
