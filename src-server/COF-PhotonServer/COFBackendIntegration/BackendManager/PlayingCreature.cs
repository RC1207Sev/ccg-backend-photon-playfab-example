﻿using COFBackendIntegration.BackendManager;
using System.Collections.Generic;

namespace COF.Server
{
    public class PlayingCreature
    {
        private short _position;
        private string _InstanceId;
        private int _Health;
        private COFPlayerProfile _Owner;
        private List<COFServerEnums.creatureStatus> _BuffsList;
        private List<CatalogCardWeapon> _Equipment = new List<CatalogCardWeapon>();
        private CatalogCardCreature _Card;

        public string InstanceId
        {
            get
            {
                return _InstanceId;
            }

            set
            {
                _InstanceId = value;
            }
        }

        public int Health
        {
            get
            {
                return _Health;
            }

            set
            {
                _Health = value;
            }
        }

        public List<COFServerEnums.creatureStatus> BuffsList
        {
            get
            {
                return _BuffsList;
            }

            set
            {
                _BuffsList = value;
            }
        }

        public short Position
        {
            get
            {
                return _position;
            }

            set
            {
                _position = value;
            }
        }

        public CatalogCardCreature Card
        {
            get
            {
                return _Card;
            }

            set
            {
                _Card = value;
            }
        }

        public List<CatalogCardWeapon> EquipList
        {
            get
            {
                return _Equipment;
            }

            set
            {
                _Equipment = value;
            }
        }

        public COFPlayerProfile Owner
        {
            get
            {
                return _Owner;
            }

            set
            {
                _Owner = value;
            }
        }

        public int GetCalculatedDamage()
        {
            int totalDamage = _Card.attack;

            foreach (CatalogCardWeapon equip in EquipList)
            {
                totalDamage += equip.attack;
            }

            return totalDamage;

        }

        public void AddEquipment(CatalogCardWeapon cardToAdd)
        {
            _Health += cardToAdd.HP;
            _Equipment.Add(cardToAdd);
        }

        public int Damage(int amount)
        {
            _Health -= amount;

            return amount;
        }

        public int Heal(int amount)
        {
            _Health += amount;

            return amount;
        }

        public bool AddStatus(COFServerEnums.creatureStatus status)
        {

            if (BuffsList.Contains(status))
                return false;
            else
                BuffsList.Add(status);

            return true;

        }

        public PlayingCreature(COFPlayerProfile pOwner, short pPosition, string pInstanceId, CatalogCardCreature pCard, int pHealth, List<COFServerEnums.creatureStatus> pBuffsList)
        {
            this._position = pPosition;
            this._InstanceId = pInstanceId;
            this._Card = pCard;
            this._Health = pHealth;

            if (pBuffsList != null)
                this._BuffsList = pBuffsList;
            else
                _BuffsList = new List<COFServerEnums.creatureStatus>();
            Owner = pOwner;
        }


    }
}