﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static COFBackendIntegration.BackendManager.COFServerEnums;

namespace COFBackendIntegration.BackendManager
{
    public class PhotonExecutionResult
    {
        public bool isSuccess;

        public errorCodes errorCode;


        public PhotonExecutionResult(bool result, errorCodes errCode = errorCodes.none)
        {
            isSuccess = result;
            errCode = errorCode;
        }

    }
}
