﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using COF.Base;
using static COFBackendIntegration.BackendManager.COFServerEnums;
using COFBackendIntegration.PhotonActions;

namespace COF.Server
{
    //!  Class of the Dungeon Status
    /*!
      This class will keep all the information related to the Dungeon. 
      Players, dungeon name, etc...
    */
    public class DungeonState
    {

        public string name;                 /*!< Dungeon name */

        public COFPlayerProfile player;     /*!< Main Player */

        public COFPlayerProfile enemy;      /*!< Second Player (Server AI) */

        //! Get Target from lineup index.
        /*!
          This method will return the position of a creature in form of a battleTarget 
          starting from it's index position in COFPlayerProfile.lineup.
            \param side which lineup has to be checked (player/enemy)
            \param index index position in COFPlayerProfile.lineup
            \return position of a creature as battleTarget 
        */
        public battleTarget TargetPositionFromLineupIndex(playingSide side, int index)
        { 
            if (side == playingSide.player)
            {
                switch (index)
                {
                    case 0:
                        return battleTarget.position1_a;
                    case 1:
                        return battleTarget.position2_a;
                    case 2:
                        return battleTarget.position3_a;
                    case 3:
                        return battleTarget.position4_a;
                    case 4:
                        return battleTarget.position5_a;
                    default:
                        return battleTarget.none;
                }
            }
            else
            {
                switch (index)
                {
                    case 0:
                        return battleTarget.position1_b;
                    case 1:
                        return battleTarget.position2_b;
                    case 2:
                        return battleTarget.position3_b;
                    case 3:
                        return battleTarget.position4_b;
                    case 4:
                        return battleTarget.position5_b;
                    default:
                        return battleTarget.none;
                }
            }
        }

        public PlayingCreature CreatureFromBattleTarget(battleTarget target)
        {
            switch (target)
            {
                case battleTarget.position1_a:
                    return player.lineup[0];
                case battleTarget.position2_a:
                    return player.lineup[1];
                case battleTarget.position3_a:
                    return player.lineup[2];
                case battleTarget.position4_a:
                    return player.lineup[3];
                case battleTarget.position5_a:
                    return player.lineup[4];
                case battleTarget.position1_b:
                    return enemy.lineup[0];
                case battleTarget.position2_b:
                    return enemy.lineup[1];
                case battleTarget.position3_b:
                    return enemy.lineup[2];
                case battleTarget.position4_b:
                    return enemy.lineup[3];
                case battleTarget.position5_b:
                    return enemy.lineup[4];
                default:
                    return null;

            }
        }

        public battleTarget TargetPositionFromCreature(PlayingCreature creature)
        {
            if (creature.Owner == this.player)
            {
                return TargetPositionFromLineupIndex(playingSide.player, creature.Position);
            }
            else
            {
                return TargetPositionFromLineupIndex(playingSide.enemy, creature.Position);
            }
        }

        public COFPlayerProfile GetPlayerOwnerFromBattlePosition(battleTarget target)
        {
            switch (target)
            {
                case battleTarget.player:
                case battleTarget.position1_a:
                case battleTarget.position2_a:
                case battleTarget.position3_a:
                case battleTarget.position4_a:
                case battleTarget.position5_a:
                    return player;

                case battleTarget.enemy:
                case battleTarget.position1_b:
                case battleTarget.position2_b:
                case battleTarget.position3_b:
                case battleTarget.position4_b:
                case battleTarget.position5_b:
                    return enemy;

                default:
                    return null;
            }
        }

        public int GetIndexPositionFromBattlePosition(battleTarget target)
        {
            switch (target)
            {
                case battleTarget.position1_a:
                case battleTarget.position1_b:
                    return 0;
                case battleTarget.position2_a:
                case battleTarget.position2_b:
                    return 1;
                case battleTarget.position3_a:
                case battleTarget.position3_b:
                    return 2;
                case battleTarget.position4_a:
                case battleTarget.position4_b:
                    return 3;
                case battleTarget.position5_a:
                case battleTarget.position5_b:
                    return 4;

                default:
                    return -1;
            }
        }

        public PlayingCreature GetLanePartner(PlayingCreature creature)
        {
            if (creature == null)
                return null;

            switch (creature.Position)
            {
                case 0:
                    return creature.Owner.lineup[1];
                case 1:
                    return creature.Owner.lineup[0];
                case 3:
                    return creature.Owner.lineup[4];
                case 4:
                    return creature.Owner.lineup[3];

                default:
                    return null;
            }
        }

        public List<COFPhotonAction> Fight(battleTarget playerSideCreature, battleTarget enemySideCreature, out int fightDelay)
        {
            fightDelay = 1;

            List<COFPhotonAction> result = new List<COFPhotonAction>();

            PlayingCreature currentPlayerCreature, currentEnemyCreature, lanePartner, playerDamagedCreature, enemyDamagedCreature;

            int playerCreatureTrueDamage = 0, enemyCreatureTrueDamage = 0;

            currentPlayerCreature = CreatureFromBattleTarget(playerSideCreature);
            currentEnemyCreature = CreatureFromBattleTarget(enemySideCreature);

            // get creature damage counting buffs and equip
            // TODO: for now takes in consideration only equip
            if (currentPlayerCreature != null)
                playerCreatureTrueDamage = currentPlayerCreature.GetCalculatedDamage();

            if (currentEnemyCreature != null)
                enemyCreatureTrueDamage = currentEnemyCreature.GetCalculatedDamage();


            // If player Creature is missing, enemy creature will attack enemy hero
            // otherwise it will damage enemy Creature
            if (currentPlayerCreature != null)
            {
                if (currentEnemyCreature != null) // both creatures are present
                {

                    // get player's lane partner
                    lanePartner = GetLanePartner(currentPlayerCreature);

                    // if the opposite creature has a lane partner with an active challenging shout,
                    // the attack has to be deviated to him
                    if ((lanePartner != null) && (lanePartner.BuffsList.Contains(creatureStatus.challenging)) && (!lanePartner.BuffsList.Contains(creatureStatus.silenced)))
                    {
                        lanePartner.Health -= enemyCreatureTrueDamage;
                        // add enemy's creature damage as photonAction
                        result.Add(new COFPhotonActionCreatureDamage(TargetPositionFromLineupIndex(playingSide.enemy, currentEnemyCreature.Position), TargetPositionFromLineupIndex(playingSide.player, lanePartner.Position), enemyCreatureTrueDamage));

                        playerDamagedCreature = lanePartner;
                    }
                    else
                    {
                        currentPlayerCreature.Health -= enemyCreatureTrueDamage;
                        // add enemy's creature damage as photonAction
                        result.Add(new COFPhotonActionCreatureDamage(TargetPositionFromLineupIndex(playingSide.enemy, currentEnemyCreature.Position), TargetPositionFromLineupIndex(playingSide.player, currentPlayerCreature.Position), enemyCreatureTrueDamage));

                        playerDamagedCreature = currentPlayerCreature;
                    }

                    // get enemy's lane partner
                    lanePartner = GetLanePartner(currentEnemyCreature);

                    // if the opposite creature has a lane partner with an active challenging shout,
                    // the attack has to be deviated to him
                    if ((lanePartner != null) && (lanePartner.BuffsList.Contains(creatureStatus.challenging)) && (!lanePartner.BuffsList.Contains(creatureStatus.silenced)))
                    {
                        lanePartner.Health -= playerCreatureTrueDamage;
                        // add enemy's creature damage as photonAction
                        result.Add(new COFPhotonActionCreatureDamage(TargetPositionFromLineupIndex(playingSide.player, currentPlayerCreature.Position), TargetPositionFromLineupIndex(playingSide.enemy, lanePartner.Position), playerCreatureTrueDamage));

                        enemyDamagedCreature = lanePartner;
                    }
                    else
                    {
                        currentEnemyCreature.Health -= playerCreatureTrueDamage;
                        // add enemy's creature damage as photonAction
                        result.Add(new COFPhotonActionCreatureDamage(TargetPositionFromLineupIndex(playingSide.player, currentPlayerCreature.Position), TargetPositionFromLineupIndex(playingSide.enemy, currentEnemyCreature.Position), playerCreatureTrueDamage));

                        enemyDamagedCreature = currentEnemyCreature;
                    }

                    // remove dead Creatures
                    if (playerDamagedCreature.Health <= 0)
                    {
                        // add to graveyard
                        player.graveyard.Add(playerDamagedCreature.Card);

                        player.lineup[playerDamagedCreature.Position] = null;

                        // add death photonAction
                        result.Add(new COFPhotonActionCreatureDeath(TargetPositionFromLineupIndex(playingSide.player, playerDamagedCreature.Position)));

                        // check effects on death
                        if (!playerDamagedCreature.BuffsList.Contains(creatureStatus.silenced))
                            result.AddRange(COFSkill.Execute(playerDamagedCreature.Owner, this, playerDamagedCreature.Card, battleTarget.none, cardSkillExecutionTime.finalAct));

                    }

                    if (enemyDamagedCreature.Health <= 0)
                    {
                        // add to graveyard
                        enemy.graveyard.Add(enemyDamagedCreature.Card);

                        enemy.lineup[enemyDamagedCreature.Position] = null;

                        // add death photonAction
                        result.Add(new COFPhotonActionCreatureDeath(TargetPositionFromLineupIndex(playingSide.enemy, enemyDamagedCreature.Position)));

                        // check effects on death.
                        if (!enemyDamagedCreature.BuffsList.Contains(creatureStatus.silenced))
                            result.AddRange(COFSkill.Execute(enemyDamagedCreature.Owner, this, enemyDamagedCreature.Card, battleTarget.none, cardSkillExecutionTime.finalAct));

                    }

                }
                else // only player creature is present
                {
                    // get enemy's lane partner
                    lanePartner = GetLanePartner(currentEnemyCreature);

                    // if the opposite creature has a lane partner with an active challenging shout,
                    // the attack has to be deviated to him
                    if ((lanePartner != null) && (lanePartner.BuffsList.Contains(creatureStatus.challenging)) && (!lanePartner.BuffsList.Contains(creatureStatus.silenced)))
                    {
                        lanePartner.Health -= playerCreatureTrueDamage;
                        // add enemy's creature damage as photonAction
                        result.Add(new COFPhotonActionCreatureDamage(TargetPositionFromLineupIndex(playingSide.player, currentPlayerCreature.Position), TargetPositionFromLineupIndex(playingSide.enemy, lanePartner.Position), playerCreatureTrueDamage));

                        enemyDamagedCreature = lanePartner;

                        if (enemyDamagedCreature.Health <= 0)
                        {
                            // add to graveyard
                            enemy.graveyard.Add(enemyDamagedCreature.Card);

                            enemy.lineup[enemyDamagedCreature.Position] = null;

                            // add death photonAction
                            result.Add(new COFPhotonActionCreatureDeath(TargetPositionFromLineupIndex(playingSide.enemy, enemyDamagedCreature.Position)));

                            // check effects on death.
                            if (!enemyDamagedCreature.BuffsList.Contains(creatureStatus.silenced))
                                result.AddRange(COFSkill.Execute(enemyDamagedCreature.Owner, this, enemyDamagedCreature.Card, battleTarget.none, cardSkillExecutionTime.finalAct));

                        }

                    }
                    else
                    {
                        enemy.health -= playerCreatureTrueDamage;
                        // add enemy's creature damage as photonAction
                        result.Add(new COFPhotonActionCreatureDamage(TargetPositionFromLineupIndex(playingSide.player, currentPlayerCreature.Position), battleTarget.enemy, playerCreatureTrueDamage));
                    }
                    
                }
            }
            else
            {
                if (currentEnemyCreature != null) // only enemy creature is present
                {
                    // get player's lane partner
                    lanePartner = GetLanePartner(currentPlayerCreature);

                    // if the opposite creature has a lane partner with an active challenging shout,
                    // the attack has to be deviated to him
                    if ((lanePartner != null) && (lanePartner.BuffsList.Contains(creatureStatus.challenging)) && (!lanePartner.BuffsList.Contains(creatureStatus.silenced)))
                    {
                        lanePartner.Health -= enemyCreatureTrueDamage;
                        // add enemy's creature damage as photonAction
                        result.Add(new COFPhotonActionCreatureDamage(TargetPositionFromLineupIndex(playingSide.enemy, currentEnemyCreature.Position), TargetPositionFromLineupIndex(playingSide.player, lanePartner.Position), enemyCreatureTrueDamage));

                        playerDamagedCreature = lanePartner;

                        // remove dead Creatures
                        if (playerDamagedCreature.Health <= 0)
                        {
                            // add to graveyard
                            player.graveyard.Add(playerDamagedCreature.Card);

                            player.lineup[playerDamagedCreature.Position] = null;

                            // add death photonAction
                            result.Add(new COFPhotonActionCreatureDeath(TargetPositionFromLineupIndex(playingSide.player, playerDamagedCreature.Position)));

                            // check effects on death
                            if (!playerDamagedCreature.BuffsList.Contains(creatureStatus.silenced))
                                result.AddRange(COFSkill.Execute(playerDamagedCreature.Owner, this, playerDamagedCreature.Card, battleTarget.none, cardSkillExecutionTime.finalAct));

                        }

                    }
                    else
                    {
                        player.health -= enemyCreatureTrueDamage;  // ((CatalogCardCreature)currentEnemyCreature.Card).attack;

                        // add enemy's creature damage as photonAction
                        result.Add(new COFPhotonActionCreatureDamage(TargetPositionFromLineupIndex(playingSide.enemy, currentEnemyCreature.Position), battleTarget.player, enemyCreatureTrueDamage));
                    }

                    
                }
                else
                {
                    // if both creatures are missing, nothing should be done
                    fightDelay = 0;
                }

            }

            return result;
        }
        
    }

    public enum playingSide : byte
    {
        player,
        enemy
    }

    }
