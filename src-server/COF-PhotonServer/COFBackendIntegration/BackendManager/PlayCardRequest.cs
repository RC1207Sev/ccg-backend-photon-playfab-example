﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static COFBackendIntegration.BackendManager.COFServerEnums;

namespace COFBackendIntegration.BackendManager
{
    public class PlayCardRequest
    {
        public string instanceId;
        public COFServerEnums.battleTarget target;

        public PlayCardRequest()
        {

        }

        public PlayCardRequest(string pInstanceId, battleTarget pTarget)
        {
            instanceId = pInstanceId;
            target = pTarget;
        }

        public static byte[] SerializePlayCardRequest(object customobject)
        {

            PlayCardRequest pcr = (PlayCardRequest)customobject;

            byte[] bytes, instanceId, target, tempbytes;

            tempbytes = Encoding.UTF8.GetBytes(pcr.instanceId);

            //instanceId = new byte[3 + tempbytes.Length];

            //target = new byte[2];

            //Protocol.Serialize(pcr.instanceId).CopyTo(instanceId, 2);

            //instanceId[0] = 0;

            bytes = new byte[tempbytes.Length + 1];

            tempbytes.CopyTo(bytes, 0);

            bytes[bytes.Length - 1] = (byte)pcr.target;

            return bytes;
        }

        public static object DeserializePlayCardRequest(byte[] bytes)
        {
            PlayCardRequest pcr = new PlayCardRequest();

            byte[] instanceId = new byte[bytes.Length - 1];

            System.Array.Copy(bytes, instanceId, bytes.Length - 1);

            pcr.instanceId = Encoding.UTF8.GetString(instanceId);
            pcr.target = (COFServerEnums.battleTarget)bytes[bytes.Length - 1];

            return pcr;
        }
    }
}
