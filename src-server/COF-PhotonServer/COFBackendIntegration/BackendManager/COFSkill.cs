﻿using COF.Base;
using COFBackendIntegration.BackendManager;
using COFBackendIntegration.PhotonActions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace COF.Server
{

    //!  Card Skills 
    /*!
      The static part of this class contains the mechanics for all the skills
      while the dynamic part describes the necessary data to recognize the skill 
      a card can execute.
      This skill can be executed in different moments (impact, finalAct, always, limitedTurns, etc..)
    */
    public class COFSkill
    {

        public COFServerEnums.cardSkill skillType;                          /*!< Name of the skill */

        public COFServerEnums.cardSkillExecutionTime skillExecution;        /*!< Execution time of the skill */

        public COFServerEnums.cardSkillTarget skillTarget;                  /*!< Skill target kind (friendly, enemy, random, etc..) */

        public object skillParameter;                                       /*!< PSkill parameter. Can be an integer (e.g. damage amount) or a string (e.g. draw a card) */

        private static uint amountOfCardsGeneratedByServer = 0;

        //! Constructor
        /*!
          Will convert received strings from the network to the correct enum value.
            \param type skill name
            \param execTime when the skill should be executed
            \param parameter could be a number (e.g. damage amount) or a string (e.g. a card name)
            \param target target type (friendly, specific, random, etc..)
        */
        public COFSkill(string type, string execTime, string parameter, string target)
        {

            skillType = StringToCardSkill(type);

            skillExecution = StringToSkillExecution(execTime);

            skillTarget = StringToSkillTarget(target);

            skillParameter = parameter;

        }

        private COFServerEnums.cardSkill StringToCardSkill(string cardskill)
        {
            switch (cardskill)
            {
                case "none":
                    return COFServerEnums.cardSkill.none;
                case "dealDamage":
                    return COFServerEnums.cardSkill.dealDamage;
                case "increaseAttack":
                    return COFServerEnums.cardSkill.increaseAttack;
                case "challengingShout":
                    return COFServerEnums.cardSkill.challengingShout;
                case "drawCards":
                    return COFServerEnums.cardSkill.drawCards;
                case "restoreHealth":
                    return COFServerEnums.cardSkill.restoreHealth;
                case "killWoundedCreature":
                    return COFServerEnums.cardSkill.killWoundedCreature;
                case "addSpell":
                    return COFServerEnums.cardSkill.addSpell;
                case "decreaseCostNextWeapon":
                    return COFServerEnums.cardSkill.decreaseCostNextWeapon;
                case "decreaseCostSelectedCard":
                    return COFServerEnums.cardSkill.decreaseCostSelectedCard;
                case "synergy":
                    return COFServerEnums.cardSkill.synergy;
                case "SwitchCreaturePosition":
                    return COFServerEnums.cardSkill.SwitchCreaturePosition;
                case "restoreMana":
                    return COFServerEnums.cardSkill.restoreMana;
                case "silence":
                    return COFServerEnums.cardSkill.silence;

                default:
                    return COFServerEnums.cardSkill.none;
            }
        }

        private COFServerEnums.cardSkillExecutionTime StringToSkillExecution(string cardskill)
        {
            switch (cardskill)
            {
                case "none":
                    return COFServerEnums.cardSkillExecutionTime.none;
                case "impact":
                    return COFServerEnums.cardSkillExecutionTime.impact;
                case "finalAct":
                    return COFServerEnums.cardSkillExecutionTime.finalAct;
                case "always":
                    return COFServerEnums.cardSkillExecutionTime.always;
                case "turns":
                    return COFServerEnums.cardSkillExecutionTime.turns;
                default:
                    return COFServerEnums.cardSkillExecutionTime.none;

            }

        }

        private COFServerEnums.cardSkillTarget StringToSkillTarget(string cardskill)
        {
            switch (cardskill)
            {
                case "none":
                    return COFServerEnums.cardSkillTarget.none;
                case "caster":
                    return COFServerEnums.cardSkillTarget.caster;
                case "enemy_hero":
                    return COFServerEnums.cardSkillTarget.enemy_hero;
                case "self":
                    return COFServerEnums.cardSkillTarget.self;
                case "friendly":
                    return COFServerEnums.cardSkillTarget.friendly;
                case "enemy_creature":
                    return COFServerEnums.cardSkillTarget.enemy_creature;
                case "all_friendly":
                    return COFServerEnums.cardSkillTarget.all_friendly;
                case "all_enemy":
                    return COFServerEnums.cardSkillTarget.all_enemy;
                case "all_heroes":
                    return COFServerEnums.cardSkillTarget.all_heroes;
                case "all_creatures":
                    return COFServerEnums.cardSkillTarget.all_creatures;
                case "all":
                    return COFServerEnums.cardSkillTarget.all;
                case "random":
                    return COFServerEnums.cardSkillTarget.random;
                case "random_enemy":
                    return COFServerEnums.cardSkillTarget.random_enemy;
                case "random_friendly":
                    return COFServerEnums.cardSkillTarget.random_friendly;
                case "lane":
                    return COFServerEnums.cardSkillTarget.lane;

                default:
                    return COFServerEnums.cardSkillTarget.none;
            }
        }

        public static COFServerEnums.creatureStatus StringToStatus(string status)
        {
            switch (status)
            {
                case "challenging":
                    return COFServerEnums.creatureStatus.challenging;
                case "stunned":
                    return COFServerEnums.creatureStatus.stunned;
                case "silenced":
                    return COFServerEnums.creatureStatus.silenced;
                case "immune":
                    return COFServerEnums.creatureStatus.immune;
                default:
                    return COFServerEnums.creatureStatus.none;
            }
        }

        public static List<COFPhotonAction> Execute(COFPlayerProfile player, DungeonState dungeon, CatalogBaseCard card, COFServerEnums.battleTarget specificTarget, COFServerEnums.cardSkillExecutionTime gamePhase)
        {
            List<COFPhotonAction> skillActions = new List<COFPhotonAction>();

            List<PlayingCreature> targets = new List<PlayingCreature>();

            List<COFPlayerProfile> targetsHeroes = new List<COFPlayerProfile>();

            // TODO: check if the target is valid
            GetTargets(dungeon, player, card.cardSkill.skillTarget, specificTarget, out targets, out targetsHeroes);

            // if the card should be played in this game phase
            if (card.cardSkill.skillExecution == gamePhase)
            {
                switch (card.cardSkill.skillType)
                {
                    case COFServerEnums.cardSkill.none:
                        break;
                    // Deal skillParam amount of damage to the target
                    case COFServerEnums.cardSkill.dealDamage:
                        skillActions.AddRange(DealDamage(player, dungeon, card.cardSkill, specificTarget));
                        break;
                    case COFServerEnums.cardSkill.increaseAttack:
                        skillActions.AddRange(IncreaseAttack(player, dungeon, card.cardSkill, specificTarget));
                        break;
                    case COFServerEnums.cardSkill.challengingShout:
                        skillActions.AddRange(AddStatus(player, dungeon, card.cardSkill, COFServerEnums.creatureStatus.challenging, targets, targetsHeroes));
                        break;
                    case COFServerEnums.cardSkill.drawCards:
                        skillActions.AddRange(DrawCards(player, dungeon, card.cardSkill, card.itemInstanceID));
                        break;
                    case COFServerEnums.cardSkill.restoreHealth:
                        skillActions.AddRange(RestoreHealth(player, dungeon, card.cardSkill, specificTarget));
                        break;
                    case COFServerEnums.cardSkill.killWoundedCreature:
                        skillActions.AddRange(KillCreature(player, dungeon, card.cardSkill, specificTarget));
                        break;
                    case COFServerEnums.cardSkill.addSpell:
                        break;
                    case COFServerEnums.cardSkill.decreaseCostNextWeapon:
                        break;
                    case COFServerEnums.cardSkill.decreaseCostSelectedCard:
                        break;
                    case COFServerEnums.cardSkill.synergy:
                        break;
                    case COFServerEnums.cardSkill.SwitchCreaturePosition:
                        skillActions.AddRange(SwitchCreaturePosition(player, dungeon, card.cardSkill, specificTarget));
                        break;
                    case COFServerEnums.cardSkill.restoreMana:
                        skillActions.AddRange(RestoreMana(player, dungeon, card.cardSkill, specificTarget)); 
                        break;
                    case COFServerEnums.cardSkill.silence:
                        //skillActions.AddRange(Silence(player, dungeon, card.cardSkill, specificTarget));
                        skillActions.AddRange(AddStatus(player, dungeon, card.cardSkill, COFServerEnums.creatureStatus.silenced, targets, targetsHeroes));
                        break;
                    case COFServerEnums.cardSkill.addStatus:

                        COFServerEnums.creatureStatus statusToAdd = StringToStatus((string)card.cardSkill.skillParameter);

                        if (statusToAdd != COFServerEnums.creatureStatus.none)
                            skillActions.AddRange(AddStatus(player, dungeon, card.cardSkill, statusToAdd, targets, targetsHeroes));

                        break;
                    case COFServerEnums.cardSkill.ResurrectCreature:
                        break;

                    default:
                        break;
                }
            }

            // TODO: this is a little inefficient: should return null 
            // if no actions are executed (and so the caller should check the result before AddRange())
            return skillActions;
        }

        private static List<COFPhotonAction> DealDamage(COFPlayerProfile player, DungeonState dungeon, COFSkill skill, COFServerEnums.battleTarget specificTarget)
        {
            List<COFPhotonAction> skillActions = new List<COFPhotonAction>();

            List<COFPhotonAction> skillRecursiveActions;

            List<PlayingCreature> targets = new List<PlayingCreature>();

            List<COFPlayerProfile> targetsHeroes = new List<COFPlayerProfile>();

            COFPlayerProfile targetPlayer;

            int damage = 0;
            int trueDamage = 0;

            damage = Int32.Parse((string)skill.skillParameter);

            if (player == dungeon.player)
                targetPlayer = dungeon.enemy;
            else
                targetPlayer = dungeon.player;

            GetTargets(dungeon, player, skill.skillTarget, specificTarget, out targets, out targetsHeroes);

            // execute damage for each single target creature
            foreach (PlayingCreature singleTarget in targets)
            {
                trueDamage = singleTarget.Damage(damage);

                skillActions.Add(new COFPhotonActionCreatureDamage(player == dungeon.player ? COFServerEnums.battleTarget.player : COFServerEnums.battleTarget.enemy,
                                                                    dungeon.TargetPositionFromLineupIndex(targetPlayer == dungeon.player ? playingSide.player : playingSide.enemy, singleTarget.Position),
                                                                    trueDamage));

                // if creature is dead, execute creature's skill on death if any
                if (singleTarget.Health <= 0)
                {

                    skillActions.Add(new COFPhotonActionCreatureDeath(dungeon.TargetPositionFromLineupIndex(targetPlayer == dungeon.player ? playingSide.player : playingSide.enemy, singleTarget.Position)));

                    // if card is not silenced
                    if (!singleTarget.BuffsList.Contains(COFServerEnums.creatureStatus.silenced))
                    {
                        skillRecursiveActions = Execute(player, dungeon, singleTarget.Card, COFServerEnums.battleTarget.none, COFServerEnums.cardSkillExecutionTime.finalAct);

                        if (skillRecursiveActions != null)
                            skillActions.AddRange(skillRecursiveActions);
                    }
 
                }
            }

            // execute damage for each target hero
            foreach (COFPlayerProfile singleTarget in targetsHeroes)
            {
                trueDamage = singleTarget.Damage(damage);

                skillActions.Add(new COFPhotonActionCreatureDamage(player == dungeon.player ? COFServerEnums.battleTarget.player : COFServerEnums.battleTarget.enemy,
                                                                    targetPlayer == dungeon.player ? COFServerEnums.battleTarget.player : COFServerEnums.battleTarget.enemy,
                                                                    trueDamage));

                // TODO: check recursive skills or player death
            }

            return skillActions;
        }

        private static List<COFPhotonAction> RestoreHealth(COFPlayerProfile player, DungeonState dungeon, COFSkill skill, COFServerEnums.battleTarget specificTarget)
        {
            List<COFPhotonAction> skillActions = new List<COFPhotonAction>();

            List<PlayingCreature> targets = new List<PlayingCreature>();

            List<COFPlayerProfile> targetsHeroes = new List<COFPlayerProfile>();

            COFPlayerProfile targetPlayer;

            int health = 0;
            int trueHealing = 0;

            health = Int32.Parse((string)skill.skillParameter);

            if (player == dungeon.player)
                targetPlayer = dungeon.enemy;
            else
                targetPlayer = dungeon.player;

            GetTargets(dungeon, player, skill.skillTarget, specificTarget, out targets, out targetsHeroes);

            // execute healing for each single target creature
            foreach (PlayingCreature singleTarget in targets)
            {
                trueHealing = singleTarget.Heal(health);

                skillActions.Add(new COFPhotonActionHealing(player == dungeon.player ? COFServerEnums.battleTarget.player : COFServerEnums.battleTarget.enemy,
                                                                    dungeon.TargetPositionFromCreature(singleTarget),
                                                                    -trueHealing));

            }

            // execute healing for each target hero
            foreach (COFPlayerProfile singleTarget in targetsHeroes)
            {
                trueHealing = singleTarget.Heal(health);

                skillActions.Add(new COFPhotonActionHealing(player == dungeon.player ? COFServerEnums.battleTarget.player : COFServerEnums.battleTarget.enemy,
                                                                    targetPlayer == dungeon.player ? COFServerEnums.battleTarget.player : COFServerEnums.battleTarget.enemy,
                                                                    -trueHealing));
            }

            return skillActions;
        }

        private static List<COFPhotonAction> KillCreature(COFPlayerProfile player, DungeonState dungeon, COFSkill skill, COFServerEnums.battleTarget specificTarget)
        {
            List<COFPhotonAction> skillActions = new List<COFPhotonAction>();

            List<COFPhotonAction> skillRecursiveActions;

            List<PlayingCreature> targets = new List<PlayingCreature>();

            List<COFPlayerProfile> targetsHeroes = new List<COFPlayerProfile>();


            GetTargets(dungeon, player, skill.skillTarget, specificTarget, out targets, out targetsHeroes);

            // execute skill for each single target creature
            foreach (PlayingCreature singleTarget in targets)
            {
                // if creature is wounded
                if ((singleTarget != null) && (singleTarget.Health < singleTarget.Card.HP))
                {
                    // add death photonAction
                    skillActions.Add(new COFPhotonActionCreatureDeath(dungeon.TargetPositionFromCreature(singleTarget)));

                    // if card is not silenced
                    if (!singleTarget.BuffsList.Contains(COFServerEnums.creatureStatus.silenced))
                    {
                        // check and execute creature's skill on finalAct
                        skillRecursiveActions = Execute(player, dungeon, singleTarget.Card, COFServerEnums.battleTarget.none, COFServerEnums.cardSkillExecutionTime.finalAct);

                        if (skillRecursiveActions != null)
                            skillActions.AddRange(skillRecursiveActions);
                    }

                }
            }

            return skillActions;
        }

        private static List<COFPhotonAction> RestoreMana(COFPlayerProfile player, DungeonState dungeon, COFSkill skill, COFServerEnums.battleTarget specificTarget)
        {
            List<COFPhotonAction> skillActions = new List<COFPhotonAction>();

            List<PlayingCreature> targets = new List<PlayingCreature>();

            List<COFPlayerProfile> targetsHeroes = new List<COFPlayerProfile>();

            int mana;

            mana = Int32.Parse((string)skill.skillParameter);

            GetTargets(dungeon, player, skill.skillTarget, specificTarget, out targets, out targetsHeroes);

            // execute damage for each target hero
            foreach (COFPlayerProfile singleTarget in targetsHeroes)
            {
                singleTarget.AddMana(mana);

                skillActions.Add(new COFPhotonActionChangeMana(player == dungeon.player ? COFServerEnums.battleTarget.player : COFServerEnums.battleTarget.enemy,
                                                    singleTarget == dungeon.player ? COFServerEnums.battleTarget.player : COFServerEnums.battleTarget.enemy,
                                                    mana));
            }

            return skillActions;

        }

        private static List<COFPhotonAction> Silence(COFPlayerProfile player, DungeonState dungeon, COFSkill skill, COFServerEnums.battleTarget specificTarget)
        {
            List<COFPhotonAction> skillActions = new List<COFPhotonAction>();

            List<PlayingCreature> targets = new List<PlayingCreature>();

            List<COFPlayerProfile> targetsHeroes = new List<COFPlayerProfile>();

            GetTargets(dungeon, player, skill.skillTarget, specificTarget, out targets, out targetsHeroes);

            // execute damage for each target creature
            foreach (PlayingCreature singleTarget in targets)
            {
                singleTarget.AddStatus(COFServerEnums.creatureStatus.silenced);

                skillActions.Add(new COPhotonActionAddedStatus(player == dungeon.player ? COFServerEnums.battleTarget.player : COFServerEnums.battleTarget.enemy,
                            dungeon.TargetPositionFromCreature(singleTarget), 
                            COFServerEnums.creatureStatus.silenced));

            }

            return skillActions;

        }

        private static List<COFPhotonAction> SwitchCreaturePosition(COFPlayerProfile player, DungeonState dungeon, COFSkill skill, COFServerEnums.battleTarget specificTarget)
        {
            List<COFPhotonAction> skillActions = new List<COFPhotonAction>();

            List<PlayingCreature> targets = new List<PlayingCreature>();

            List<COFPlayerProfile> targetsHeroes = new List<COFPlayerProfile>();

            COFPlayerProfile sourcePlayer, destinationPlayer;

            int sourcePosition = 0;
            int destinationPosition = 0;

            GetTargets(dungeon, player, skill.skillTarget, specificTarget, out targets, out targetsHeroes);

            // this skill will work with only one target (if GetTargets will return more, only the first will be considered)

            if (targets.Count > 0) // TODO: this skill requires refactor of the code to accept 2 targets instead of one
            {
                PlayingCreature tempSwap = dungeon.CreatureFromBattleTarget(specificTarget);

                sourcePlayer = tempSwap.Owner;
                sourcePosition = tempSwap.Position - 1;

                destinationPosition = targets[0].Position - 1;
                destinationPlayer = targets[0].Owner;

                tempSwap = sourcePlayer.lineup[sourcePosition];
                sourcePlayer.lineup[sourcePosition] = destinationPlayer.lineup[destinationPosition];
                destinationPlayer.lineup[destinationPosition] = tempSwap;

            }

            // TODO: create photonActionMove
            skillActions.Add(new COFPhotonActionCreatureDamage(specificTarget, dungeon.TargetPositionFromCreature(targets[0]), 0));


            return skillActions;
        }

        private static List<COFPhotonAction> IncreaseAttack(COFPlayerProfile player, DungeonState dungeon, COFSkill skill, COFServerEnums.battleTarget specificTarget)
        {
            List<COFPhotonAction> skillActions = new List<COFPhotonAction>();

            List<PlayingCreature> targets = new List<PlayingCreature>();

            List<COFPlayerProfile> targetsHeroes = new List<COFPlayerProfile>();

            COFPlayerProfile targetPlayer;

            CatalogCardWeapon fakeEquipment = new CatalogCardWeapon();

            int amount = 0;

            amount = Int32.Parse((string)skill.skillParameter);

            // create a fake piece of equipment with the given attack power
            fakeEquipment.attack = amount;
            fakeEquipment.itemID = skill.skillType.ToString();
            fakeEquipment.itemInstanceID = "BUFF";

            if (player == dungeon.player)
                targetPlayer = dungeon.enemy;
            else
                targetPlayer = dungeon.player;

            GetTargets(dungeon, player, skill.skillTarget, specificTarget, out targets, out targetsHeroes);

            // execute damage for each single target creature
            foreach (PlayingCreature singleTarget in targets)
            {
                singleTarget.AddEquipment(fakeEquipment);

                // TODO?: add photonActionBuff?
                skillActions.Add(new COFPhotonActionNewEquipment(player == dungeon.player ? COFServerEnums.battleTarget.player : COFServerEnums.battleTarget.enemy,
                                                                    dungeon.TargetPositionFromCreature(singleTarget),
                                                                    fakeEquipment.itemID));
            }

            return skillActions;
        }

        private static List<COFPhotonAction> DrawCards(COFPlayerProfile player, DungeonState dungeon, COFSkill skill, string cardInstanceID)
        {
            List<COFPhotonAction> skillActions = new List<COFPhotonAction>();

            int amount = 0;
            Int32.TryParse((string)skill.skillParameter, out amount);

            COFServerEnums.battleTarget sourcePosition = player == dungeon.player ? COFServerEnums.battleTarget.player : COFServerEnums.battleTarget.enemy;
            PlayingCreature castingCard = FindCreatureFromLineupByInstanceID(player.lineup, cardInstanceID);
            if(castingCard != null)
            {
                sourcePosition = dungeon.TargetPositionFromCreature(castingCard);
            }

            for (int i=0; i<amount; i++)
            {
                ICatalogItem drawnCard = player.AddCardsOnHand();

                if (drawnCard != null)
                {
                    skillActions.Add(new COFPhotonActionCardDrawn(sourcePosition,
                                                player == dungeon.player ? COFServerEnums.battleTarget.player : COFServerEnums.battleTarget.enemy,
                                                drawnCard.itemID, drawnCard.itemInstanceID));
                }
            }

            return skillActions;
        }

        private static List<COFPhotonAction> AddStatus(COFPlayerProfile player, DungeonState dungeon, COFSkill skill, COFServerEnums.creatureStatus status, List<PlayingCreature> targets, List<COFPlayerProfile> targetsHeroes)
        {
            List<COFPhotonAction> skillActions = new List<COFPhotonAction>();

            // execute damage for each target creature
            foreach (PlayingCreature singleTarget in targets)
            {
                singleTarget.AddStatus(status);

                skillActions.Add(new COPhotonActionAddedStatus(player == dungeon.player? COFServerEnums.battleTarget.player : COFServerEnums.battleTarget.enemy,
                            dungeon.TargetPositionFromCreature(singleTarget), 
                            status));

            }

            return skillActions;
        }

        // TODO: Confirm is used by COFServerEnums.cardSkill.addSpell?
        private static List<COFPhotonAction> AddSpell(COFPlayerProfile player, DungeonState dungeon, COFSkill skill, List<PlayingCreature> targets, List<COFPlayerProfile> targetsHeroes)
        {
            List<COFPhotonAction> skillActions = new List<COFPhotonAction>();

            foreach (COFPlayerProfile singleTarget in targetsHeroes)
            {
                // create a copy of the requested card
                CatalogBaseCard newCard = (CatalogBaseCard) BackendManager.GetCatalogItemById((string)skill.skillParameter);

                if (newCard != null)
                {
                    // the itemInstanceID has to be unique, so uses a shared counter
                    newCard.itemInstanceID = "COFGBS" + amountOfCardsGeneratedByServer;

                    // TODO: should ensure this entire method is atomic
                    amountOfCardsGeneratedByServer = (amountOfCardsGeneratedByServer + 1) % UInt32.MaxValue;

                    skillActions.Add(new COFPhotonActionCardDrawn(player == dungeon.player ? COFServerEnums.battleTarget.player : COFServerEnums.battleTarget.enemy,
                                singleTarget == dungeon.player ? COFServerEnums.battleTarget.player : COFServerEnums.battleTarget.enemy,
                                newCard.itemID, newCard.itemInstanceID));

                    // add to target's hand
                    singleTarget.hand.Add(newCard);

                }


            }

            return skillActions;

        }

        private static List<PlayingCreature> GetLivingCreaturesFromLineup(PlayingCreature[] lineup)
        {
            List<PlayingCreature> result = new List<PlayingCreature>();

            foreach (PlayingCreature creature in lineup)
            {
                if ((creature != null) && (creature.Health > 0))
                    result.Add(creature);
            }

            return result;
        }

        private static PlayingCreature FindCreatureFromLineupByInstanceID(PlayingCreature[] lineup, string InstanceId)
        {
            if (lineup == null)
                return null;

            foreach (PlayingCreature creature in lineup)
            {
                if(creature != null)
                {
                    if (creature.InstanceId == InstanceId)
                        return creature;
                }
            }

            return null;
        }

        private static bool GetTargets(DungeonState dungeon, COFPlayerProfile player, COFServerEnums.cardSkillTarget targetType, COFServerEnums.battleTarget specificTarget, out List<PlayingCreature> targets, out List<COFPlayerProfile> targetsHeroes)

        {

            List<PlayingCreature> tempTargetList;

            PlayingCreature tempTarget;

            targets = new List<PlayingCreature>();

            targetsHeroes = new List<COFPlayerProfile>();

            COFPlayerProfile targetPlayer;

            if (player == dungeon.player)
                targetPlayer = dungeon.enemy;
            else
                targetPlayer = dungeon.player;

            switch (targetType)
            {
                case COFServerEnums.cardSkillTarget.all:

                    // add player's lineup
                    targets.AddRange(GetLivingCreaturesFromLineup(player.lineup));
                    // add enemy's lineup
                    targets.AddRange(GetLivingCreaturesFromLineup(targetPlayer.lineup));
                    // add caster
                    targetsHeroes.Add(player);
                    // add enemy
                    targetsHeroes.Add(targetPlayer);

                    break;

                case COFServerEnums.cardSkillTarget.all_creatures:

                    // add player's lineup
                    targets.AddRange(GetLivingCreaturesFromLineup(player.lineup));
                    // add enemy's lineup
                    targets.AddRange(GetLivingCreaturesFromLineup(targetPlayer.lineup));

                    break;

                case COFServerEnums.cardSkillTarget.all_enemy:

                    // add enemy's lineup
                    targets.AddRange(GetLivingCreaturesFromLineup(targetPlayer.lineup));

                    break;

                case COFServerEnums.cardSkillTarget.all_friendly:

                    // add player's lineup
                    targets.AddRange(GetLivingCreaturesFromLineup(player.lineup));

                    break;

                case COFServerEnums.cardSkillTarget.all_heroes:

                    // add caster
                    targetsHeroes.Add(player);
                    // add enemy
                    targetsHeroes.Add(targetPlayer);

                    break;

                case COFServerEnums.cardSkillTarget.caster:

                    // add caster
                    targetsHeroes.Add(player);

                    break;

                case COFServerEnums.cardSkillTarget.enemy_creature:

                    // get specific target
                    tempTarget = dungeon.CreatureFromBattleTarget(specificTarget);

                    if ((tempTarget != null) && (targetPlayer == tempTarget.Owner))
                        targets.Add(tempTarget);
                    else
                        return false;

                    break;

                case COFServerEnums.cardSkillTarget.enemy_hero:

                    targetsHeroes.Add(targetPlayer);

                    break;

                case COFServerEnums.cardSkillTarget.friendly:

                    // get specific target
                    tempTarget = dungeon.CreatureFromBattleTarget(specificTarget);

                    if ((tempTarget != null) && (player == tempTarget.Owner))
                        targets.Add(tempTarget);
                    else
                        return false;

                    break;

                case COFServerEnums.cardSkillTarget.lane:

                    // get lane partner (if any)

                    break;

                case COFServerEnums.cardSkillTarget.random:

                    // get player's lineup
                    tempTargetList = GetLivingCreaturesFromLineup(player.lineup);

                    // get enemy's lineup
                    tempTargetList.AddRange(GetLivingCreaturesFromLineup(targetPlayer.lineup));

                    // get only one random
                    if (tempTargetList.Count() > 0)
                        targets.Add(tempTargetList[new Random().Next(tempTargetList.Count())]);

                    break;

                case COFServerEnums.cardSkillTarget.random_enemy:

                    // get enemy's lineup
                    tempTargetList = GetLivingCreaturesFromLineup(targetPlayer.lineup);

                    // get only one random
                    if (tempTargetList.Count() > 0)
                        targets.Add(tempTargetList[new Random().Next(tempTargetList.Count())]);

                    break;

                case COFServerEnums.cardSkillTarget.random_friendly:

                    // get player's lineup
                    tempTargetList = GetLivingCreaturesFromLineup(player.lineup);

                    // get only one random
                    if (tempTargetList.Count() > 0)
                        targets.Add(tempTargetList[new Random().Next(tempTargetList.Count())]);

                    break;

                case COFServerEnums.cardSkillTarget.self:

                    // get the creature casting this skill


                    break;

                default:

                    return false;
            }

            return true;
        }
        
    }
}
