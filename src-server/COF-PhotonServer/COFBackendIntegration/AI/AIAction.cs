﻿using COF.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static COFBackendIntegration.BackendManager.COFServerEnums;

namespace COFBackendIntegration.AI
{
    public class AIAction
    {
        public string inventoryIdCardToUse;
        public battleTarget target1;
        public battleTarget target2;

        public AIAction(string InstanceId, battleTarget passedTarget, battleTarget passedSecondaryTarget = battleTarget.none)
        {
            inventoryIdCardToUse = InstanceId;
            target1 = passedTarget;
            target2 = passedSecondaryTarget;
        }
    }
}
