﻿using COF.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COFBackendIntegration.AI
{
    public class AIManager
    {

        DungeonState dungeonContext;
        COFPlayerProfile AIPlayer;

        public AIManager(DungeonState passedDungeon)
        {
            dungeonContext = passedDungeon;
            AIPlayer = passedDungeon.enemy;
        }

        public AIAction GetCardToPlay()
        {
            foreach (CatalogBaseCard singleCard in AIPlayer.hand)
            {
                // basic AI: will play the first creature it finds in the hand
                // in the first available lineup slot.
                if (singleCard.catalogItemType == typeof(CatalogCardCreature) && // if the card is a Creature
                    singleCard.manaCost < AIPlayer.mana)                         // AND the mana cost is affordable
                {
                    for (int i=0;i<5;i++)   // check creatures in his lineup
                    {
                        if (AIPlayer.lineup[i] == null)     // first empty slot
                        {
                            return new AIAction(singleCard.itemInstanceID, dungeonContext.TargetPositionFromLineupIndex(playingSide.enemy, i));
                        }
                    }
                }
                    
            }

            return null; // no action if no cards can be played

        }
    }
}
