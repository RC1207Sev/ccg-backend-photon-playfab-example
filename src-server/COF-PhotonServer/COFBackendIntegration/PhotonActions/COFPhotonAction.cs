﻿using System.Collections;
using static COFBackendIntegration.BackendManager.COFServerEnums;

namespace COFBackendIntegration.PhotonActions
{
    //!  Photon Action
    /*!
      This abstract class is the base for all possible PhotonActions.
      Photon Action is a structure to identify and send each single action 
      happened during the game. It could be a creature damaging another, 
      a buff effect, etc...
      This structure has to be used with COFPhotonActionFactory (factory pattern)

      To create a new photonAction child:

        - Create class inheriting COFPhotonAction
        - add new value on photonActionType enumerator
        - check if you need to add new fields for actionFields (needed for serialization)
        - update COFPhotonActionFactory with new type
        - update GameMechanicsSP.ExecuteCard with new type (if needed)
        - update client side

    */
    public abstract class COFPhotonAction
    {

        public photonActionType ActionType { get; protected set; }  /*!< leaf type of action extending the base class */  

        //! Convert the content of the class in an Hashtable
        /*!
          "Serializes" the class instance into an Hashtable.
          Used to easily send it thought network.
            \return Hashtable containing all fields values
        */
        public abstract Hashtable ToHashtable(); 

    }

    //! this enumerator will support Serialization and Deserialization of fields
    public enum actionFields : byte
    {
        type,
        sourcePosition,
        targetPosition,
        amount,
        cardInstanceId,
        cardId,
        effect
    }

}
