﻿using System;
using System.Collections;
using COFBackendIntegration.BackendManager;

namespace COFBackendIntegration.PhotonActions
{
    //!  Photon Action on new status
    /*!
      This action is created to inform the client that 
      a creature has a new status.
      This class implements PhotonAction.
    */
    public class COPhotonActionAddedStatus : COFPhotonAction
    {

        public COFServerEnums.battleTarget sourcePosition;     /*!< position of the creature/player casting the status */
        public COFServerEnums.battleTarget targetPosition;     /*!< position of the creature receiving the status */
        public COFServerEnums.creatureStatus status;                      /*!< added status */

        //! Constructor.
        /*!
          This constructor is used from the server since it already know which kind of action 
          needs to be created.
            \param pSource position of the creature dealing the damage
            \param position of the creature receiving the damage
            \param pStatis the new status
            \return the initialized instance of the class 
        */
        public COPhotonActionAddedStatus(COFServerEnums.battleTarget pSource, COFServerEnums.battleTarget pTarget, COFServerEnums.creatureStatus pStatus)
        {
            base.ActionType = COFServerEnums.photonActionType.addedStatus;
            sourcePosition = pSource;
            targetPosition = pTarget;
            status = pStatus;
        }

        //! Constructor.
        /*!
          This constructor is used from COFPhotonActionFactory when the Photon Action 
          is received from the network and not created locally.
            \param data Hashtable of the raw photon action received
            \return the initialized instance of the class 
        */
        public COPhotonActionAddedStatus(Hashtable data)
        {
            ActionType =        (COFServerEnums.photonActionType)data[(byte)actionFields.type];

            sourcePosition =    (COFServerEnums.battleTarget)data[(byte)actionFields.sourcePosition];
            targetPosition =    (COFServerEnums.battleTarget)data[(byte)actionFields.targetPosition];
            status =            (COFServerEnums.creatureStatus)data[(byte)actionFields.effect];
        }

        public override Hashtable ToHashtable()
        {
            Hashtable resultHash = new Hashtable();

            resultHash.Add(actionFields.type, ActionType);
            resultHash.Add(actionFields.sourcePosition, sourcePosition);
            resultHash.Add(actionFields.targetPosition, targetPosition);
            resultHash.Add(actionFields.effect, status);

            return resultHash;
        }
    }
}
