﻿using COFBackendIntegration.BackendManager;
using System.Collections;

namespace COFBackendIntegration.PhotonActions
{
    //!  Photon Action on new card drawn in game
    /*!
      This action is created to inform the client that 
      a new card has been drawn on the hand of one player
      This class implements PhotonAction.
    */
    public class COFPhotonActionCardDrawn : COFPhotonAction
    {

        public string cardInstanceId;                           /*!< card's instance id */
        public string cardId;                                   /*!< card's catalog id */
        public COFServerEnums.battleTarget sourcePosition;      /*!< who played the equipment card */
        public COFServerEnums.battleTarget targetPosition;      /*!< position of the creature with this equipment */

        //! Constructor.
        /*!
          This constructor is used from the server since it already know which kind of action 
          needs to be created.
            \param pSource who cast the drawn
            \param pTarget player receiving the drawn card
            \param pCardId Card's catalog id
            \param pCardInstanceId Card's instance id
            \return the initialized instance of the class 
        */
        public COFPhotonActionCardDrawn(COFServerEnums.battleTarget pSource, COFServerEnums.battleTarget pTarget, string pCardId, string pCardInstanceId = null)
        {
            base.ActionType = COFServerEnums.photonActionType.addedCardtoHand;
            sourcePosition = pSource;
            targetPosition = pTarget;
            cardId = pCardId;
            cardInstanceId = pCardInstanceId;
        }

        //! Constructor.
        /*!
          This constructor is used from COFPhotonActionFactory when the Photon Action 
          is received from the network and not created locally.
            \param data Hashtable of the raw photon action received
            \return the initialized instance of the class 
        */
        public COFPhotonActionCardDrawn(Hashtable data)
        {
            ActionType = (COFServerEnums.photonActionType)data[(byte)actionFields.type];

            sourcePosition = (COFServerEnums.battleTarget)data[(byte)actionFields.sourcePosition];
            targetPosition = (COFServerEnums.battleTarget)data[(byte)actionFields.targetPosition];
            cardId = (string)data[(byte)actionFields.cardId];
            cardInstanceId = (string)data[(byte)actionFields.cardInstanceId];
        }

        //! "Serializes" the instance.
        /*!
          This method is used to "serialize" the instance of the class 
          so that can be send through photon network.
            \return Hashtable representation of the instance of the class 
        */
        public override Hashtable ToHashtable()
        {
            Hashtable resultHash = new Hashtable();

            resultHash.Add(actionFields.type, ActionType);
            resultHash.Add(actionFields.sourcePosition, sourcePosition);
            resultHash.Add(actionFields.targetPosition, targetPosition);
            resultHash.Add(actionFields.cardInstanceId, cardInstanceId);
            resultHash.Add(actionFields.cardId, cardId);

            return resultHash;
        }
    }
}
