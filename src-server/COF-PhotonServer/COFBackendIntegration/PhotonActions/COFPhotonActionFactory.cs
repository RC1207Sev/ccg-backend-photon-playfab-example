﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static COFBackendIntegration.BackendManager.COFServerEnums;

namespace COFBackendIntegration.PhotonActions
{
    //!  Photon Action Factory
    /*!
      This class implements the factory pattern for creating 
      the right Photon Actions from the "serialized" Hashtable 
      received from the network.
    */
    static class COFPhotonActionFactory
    {
        //! Factory method
        /*!
          This method will create and return the right implementation of 
          PhotonAction using the COFPhotonAction.actionFields.type field.
            \param rawPhotonAction Hashtable containing the raw PhotonAction received from the network
            \return already initalized Photon Action leaf implemention
        */
        public static COFPhotonAction GetPhotonAction(Hashtable rawPhotonAction)
        {
            switch ((photonActionType)rawPhotonAction[actionFields.type])
            {
                case photonActionType.creatureDamage:

                    return new COFPhotonActionCreatureDamage(rawPhotonAction);

                case photonActionType.creatureAdded:

                    return new COFPhotonActionNewCreature(rawPhotonAction);

                case photonActionType.addedEquipment:

                    return new COFPhotonActionNewEquipment(rawPhotonAction);

                case photonActionType.creatureDeath:

                    return new COFPhotonActionCreatureDeath(rawPhotonAction);

                case photonActionType.healing:

                    return new COFPhotonActionHealing(rawPhotonAction);

                case photonActionType.changeMana:

                    return new COFPhotonActionChangeMana(rawPhotonAction);

                case photonActionType.addedCardtoHand:

                    return new COFPhotonActionCardDrawn(rawPhotonAction);

                case photonActionType.addedStatus:

                    return new COPhotonActionAddedStatus(rawPhotonAction);

                default:
                    
                    throw new NotImplementedException("COFPhotonActionFactory: PhotonAction of type " + (photonActionType)rawPhotonAction[actionFields.type] + " not found or not yet implemented.");
            }
        }
    }
}
