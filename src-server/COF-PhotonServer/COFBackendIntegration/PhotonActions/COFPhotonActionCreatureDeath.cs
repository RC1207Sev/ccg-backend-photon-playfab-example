﻿using System.Collections;
using static COFBackendIntegration.BackendManager.COFServerEnums;

namespace COFBackendIntegration.PhotonActions
{
    //!  Photon Action on Creature Death
    /*!
      This action is created to inform the client that 
      a specific creature in play died.
      This class implements PhotonAction.
    */
    public class COFPhotonActionCreatureDeath : COFPhotonAction
    {

        public battleTarget creaturePosition;     /*!< position of the death creature*/

        //! Constructor.
        /*!
          This constructor is used from the server since it already know which kind of action 
          needs to be created.
            \param pCreaturePosition position of the death creature
            \return the initialized instance of the class 
        */
        public COFPhotonActionCreatureDeath(battleTarget pCreaturePosition)
        {
            base.ActionType = photonActionType.creatureDeath;
            creaturePosition = pCreaturePosition;

        }

        //! Constructor.
        /*!
          This constructor is used from COFPhotonActionFactory when the Photon Action 
          is received from the network and not created locally.
            \param data Hashtable of the raw photon action received
            \return the initialized instance of the class 
        */
        public COFPhotonActionCreatureDeath(Hashtable data)
        {
            ActionType = (photonActionType)data[(byte)actionFields.type];

            creaturePosition = (battleTarget)data[(byte)actionFields.sourcePosition];

        }

        //! "Serializes" the instance.
        /*!
          This method is used to "serialize" the instance of the class 
          so that can be send through photon network.
            \return Hashtable representation of the instance of the class 
        */
        public override Hashtable ToHashtable()
        {
            Hashtable resultHash = new Hashtable();

            resultHash.Add(actionFields.type, ActionType);
            resultHash.Add(actionFields.sourcePosition, creaturePosition);

            return resultHash;
        }
    }
}
