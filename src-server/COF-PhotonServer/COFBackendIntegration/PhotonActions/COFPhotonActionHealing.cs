﻿using System.Collections;
using COFBackendIntegration.BackendManager;

namespace COFBackendIntegration.PhotonActions
{
    //!  Photon Action on Healing
    /*!
      This action is created to inform the client that 
      a creature has been healed by a spell or creature.
      This class implements PhotonAction.
    */
    class COFPhotonActionHealing : COFPhotonActionCreatureDamage
    {

        //! Constructor.
        /*!
          This constructor is used from the server since it already know which kind of action 
          needs to be created.
            \param pSource position of the healing creature/player
            \param position of the creature receiving the healing
            \param amount of healing received
            \return the initialized instance of the class 
        */
        public COFPhotonActionHealing(COFServerEnums.battleTarget pSource, COFServerEnums.battleTarget pTarget, int pAmount) : base(pSource, pTarget, pAmount)
        {
            base.ActionType = COFServerEnums.photonActionType.healing;
            sourcePosition = pSource;
            targetPosition = pTarget;
            amount = pAmount;
        }

        //! Constructor.
        /*!
          This constructor is used from COFPhotonActionFactory when the Photon Action 
          is received from the network and not created locally.
            \param data Hashtable of the raw photon action received
            \return the initialized instance of the class 
        */
        public COFPhotonActionHealing(Hashtable data) : base(data)
        {
            ActionType = (COFServerEnums.photonActionType)data[(byte)actionFields.type];

            sourcePosition = (COFServerEnums.battleTarget)data[(byte)actionFields.sourcePosition];
            targetPosition = (COFServerEnums.battleTarget)data[(byte)actionFields.targetPosition];
            amount = (int)data[(byte)actionFields.amount];
        }
    }
}
