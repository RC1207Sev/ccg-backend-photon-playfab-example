﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static COFBackendIntegration.BackendManager.COFServerEnums;

namespace COFBackendIntegration.PhotonActions
{
    //!  Photon Action on Creature Damage
    /*!
      This action is created to inform the client that 
      a creature delivered damage to something else.
      This class implements PhotonAction.
    */
    public class COFPhotonActionCreatureDamage : COFPhotonAction
    {

        public battleTarget sourcePosition;     /*!< position of the creature dealing the damage */
        public battleTarget targetPosition;     /*!< position of the creature receiving the damage */
        public int amount;                      /*!< amount of damage delivered */

        //! Constructor.
        /*!
          This constructor is used from the server since it already know which kind of action 
          needs to be created.
            \param pSource position of the creature dealing the damage
            \param position of the creature receiving the damage
            \param amount of damage delivered
            \return the initialized instance of the class 
        */
        public COFPhotonActionCreatureDamage(battleTarget pSource, battleTarget pTarget, int pAmount)
        {
            base.ActionType = photonActionType.creatureDamage;
            sourcePosition = pSource;
            targetPosition = pTarget;
            amount = pAmount;
        }

        //! Constructor.
        /*!
          This constructor is used from COFPhotonActionFactory when the Photon Action 
          is received from the network and not created locally.
            \param data Hashtable of the raw photon action received
            \return the initialized instance of the class 
        */
        public COFPhotonActionCreatureDamage(Hashtable data)
        {
            ActionType = (photonActionType)  data[(byte)actionFields.type];

            sourcePosition = (battleTarget)  data[(byte)actionFields.sourcePosition];
            targetPosition = (battleTarget)  data[(byte)actionFields.targetPosition];
            amount = (int)                   data[(byte)actionFields.amount];
        }

        //! "Serializes" the instance.
        /*!
          This method is used to "serialize" the instance of the class 
          so that can be send through photon network.
            \return Hashtable representation of the instance of the class 
        */
        public override Hashtable ToHashtable()
        {
            Hashtable resultHash = new Hashtable();

            resultHash.Add(actionFields.type, ActionType);
            resultHash.Add(actionFields.sourcePosition, sourcePosition);
            resultHash.Add(actionFields.targetPosition, targetPosition);
            resultHash.Add(actionFields.amount, amount);

            return resultHash;
        }
    }
}
