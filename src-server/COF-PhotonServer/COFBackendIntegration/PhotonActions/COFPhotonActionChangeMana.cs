﻿using COFBackendIntegration.BackendManager;
using System.Collections;

namespace COFBackendIntegration.PhotonActions
{

    //!  Photon Action on mana change
    /*!
      This action is created to inform the client that 
      a player's mana has been changed by a skill.
      This class implements PhotonAction.
    */
    public class COFPhotonActionChangeMana : COFPhotonActionCreatureDamage
    {
        //! Constructor.
        /*!
          This constructor is used from the server since it already know which kind of action 
          needs to be created.
            \param pSource position of the healing creature/player
            \param position of the creature receiving the healing
            \param amount of mana changed
            \return the initialized instance of the class 
        */
        public COFPhotonActionChangeMana(COFServerEnums.battleTarget pSource, COFServerEnums.battleTarget pTarget, int pAmount) : base(pSource, pTarget, pAmount)
        {
            base.ActionType = COFServerEnums.photonActionType.changeMana;
            sourcePosition = pSource;
            targetPosition = pTarget;
            amount = pAmount;
        }

        //! Constructor.
        /*!
          This constructor is used from COFPhotonActionFactory when the Photon Action 
          is received from the network and not created locally.
            \param data Hashtable of the raw photon action received
            \return the initialized instance of the class 
        */
        public COFPhotonActionChangeMana(Hashtable data) : base(data)
        {
            ActionType = (COFServerEnums.photonActionType)data[(byte)actionFields.type];

            sourcePosition = (COFServerEnums.battleTarget)data[(byte)actionFields.sourcePosition];
            targetPosition = (COFServerEnums.battleTarget)data[(byte)actionFields.targetPosition];
            amount = (int)data[(byte)actionFields.amount];
        }
    }
}
