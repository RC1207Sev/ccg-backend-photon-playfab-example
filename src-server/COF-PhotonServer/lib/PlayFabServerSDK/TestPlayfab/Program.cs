﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PlayFab;
using COF.Server;
using COF.Base;

namespace TestPlayfab
{
    class Program
    {
        static COFPlayerProfile myplayer = new COFPlayerProfile();

        static void Main(string[] args)
        {

            Console.WriteLine("Test");

            myplayer.playfabId = "1FDDDA1014E1EB0B";


            Task task = new Task(StartBackendInitialization);
            task.Start();

            Console.ReadLine();


        }

        static async void StartBackendInitialization()
        {
            await BackendManager.Instance.Init(loginMethod.zerofriction, async (result) =>
            {
                if (result.isSuccess)
                {
                    foreach (ICatalogItem item in BackendManager.Instance.catalog)
                    {
                        Console.WriteLine(item.displayName);
                    }

                    await BackendManager.Instance.GetInventory(myplayer);

                    foreach (ICatalogItem item in myplayer.inventory)
                    {
                        Console.WriteLine(item.displayName + " : " + item.itemInstanceID);
                    }

                }


            });



        }
    }
}
