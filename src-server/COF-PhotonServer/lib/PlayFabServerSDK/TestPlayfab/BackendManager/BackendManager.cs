﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using COF.Base;
using PlayFab;
using COF.Playfab;
using PlayFab.ServerModels;


namespace COF.Server
{
    public enum loginMethod
    {
        zerofriction = 1,
        fb = 2,
        gp = 3,
        ios = 4
    }

    class BackendManager
    {

        private static BackendManager instance;

        public BackendManager() { }

        public static BackendManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new BackendManager();
                }
                return instance;
            }
        }

        public string SessionTicket;
        public string PlayFabId;

        public List<ICatalogItem> catalog = new List<ICatalogItem>();

        public List<ICatalogItem> inventory = new List<ICatalogItem>();

        public List<ICatalogItem> deck = new List<ICatalogItem>();

        // Virtual Currencies
        public int gold;
        public int soulDust;
        public int energy;
        public DateTime nextEnergyRechargeTime;


        private Action<BackendInitResult> _callback = null;
        private bool isNewlyCreated = false;

        private String deckString = "[]";


        public async System.Threading.Tasks.Task<bool> Init(loginMethod login, Action<BackendInitResult> callback)
        {
            PlayFabSettings.TitleId = "81DF";
            PlayFabSettings.DeveloperSecretKey = "SQ56GN64B5RZA891WIEDFN7JBPC3NFCHSH6S71I8SH8B1BRHRX";

            PF_Bridge.OnPlayFabCallbackError += (details, method, style) =>
            {
                Console.WriteLine("Error occurred on " + method.ToString() + " with detail:" + details);
            };

            _callback = callback;

            await ConnectToPlayFab(login);

            return true;

        }

        async System.Threading.Tasks.Task<bool> ConnectToPlayFab(loginMethod lastLogin = 0)
        {

            float tempVer;
            int tempInt;
            string tempStr;
            bool tempBool = true;
            String[] itemClassLevels;

            Dictionary<string, string> custData;
            

            GetCatalogItemsRequest catalogRequest = new GetCatalogItemsRequest()
            {
                CatalogVersion = null
            };
            PlayFabResult<GetCatalogItemsResult> catalogResult = await PlayFabServerAPI.GetCatalogItemsAsync(catalogRequest);

            // meanwhile, create ICatalogItem objects
            catalog.Clear();

            foreach (CatalogItem catItem in catalogResult.Result.Catalog)
            {
                itemClassLevels = catItem.ItemClass.Split(':');

                switch (itemClassLevels[0])
                {
                    case "card":

                        switch (itemClassLevels[1]) // which kind of card we have
                        {
                            case "creature":
                                CatalogCardCreature cardCreature = new CatalogCardCreature();   //  (CatalogCardCreature) temp;

                                cardCreature = (CatalogCardCreature)InitItem(cardCreature, catItem);

                                cardCreature.catalogItemType = typeof(CatalogCardCreature);
                                
                                custData = PlayFab.Json.JsonWrapper.DeserializeObject<Dictionary<string, string>>(catItem.CustomData);

                                if (float.TryParse(custData["Version"], out tempVer))
                                    cardCreature.itemVersion = tempVer;

                                if (int.TryParse(custData["Soul Dust Disenchant Cost"], out tempInt))
                                    cardCreature.DisenchantCost = tempInt;

                                cardCreature.soundEffect = custData["Sound Effects"];

                                cardCreature.animEffect = custData["Animation Effects"];

                                if (int.TryParse(custData["Soul Dust Purchase Cost"], out tempInt))
                                    cardCreature.PurchaseCost = tempInt;

                                if ((custData.TryGetValue("disenchantable", out tempStr)))
                                    bool.TryParse(custData["disenchantable"], out tempBool);
                                else
                                    tempBool = true;

                                ((CatalogBaseCard)cardCreature).disenchantable = tempBool;

                                cardCreature.Rarity = custData["Rank"];

                                if (int.TryParse(custData["Mana Cost"], out tempInt))
                                    cardCreature.manaCost = tempInt;

                                cardCreature.Element = custData["Element"];

                                if (int.TryParse(custData["HP"], out tempInt))
                                    cardCreature.HP = tempInt;

                                cardCreature.dungeon = custData["Dungeon"];

                                cardCreature.skill = custData["Skill"];

                                if (int.TryParse(custData["ATK"], out tempInt))
                                    cardCreature.attack = tempInt;

                                cardCreature.classCreature = custData["Class"];

                                catalog.Add(cardCreature);

                                break;

                            case "spell":
                                CatalogCardSpell cardSpell = new CatalogCardSpell(); // (CatalogCardSpell) temp;

                                cardSpell = (CatalogCardSpell)InitItem(cardSpell, catItem);

                                cardSpell.catalogItemType = typeof(CatalogCardSpell);

                                custData = PlayFab.Json.JsonWrapper.DeserializeObject<Dictionary<string, string>>(catItem.CustomData);

                                if (float.TryParse(custData["Version"], out tempVer))
                                    cardSpell.itemVersion = tempVer;

                                if (int.TryParse(custData["Soul Dust Disenchant Cost"], out tempInt))
                                    cardSpell.DisenchantCost = tempInt;

                                if ((custData.TryGetValue("disenchantable", out tempStr)))
                                    bool.TryParse(custData["disenchantable"], out tempBool);
                                else
                                    tempBool = true;

                                ((CatalogBaseCard)cardSpell).disenchantable = tempBool;

                                // TODO: once we will have the sound and animation effects, decomment next 2 lines

                                //cardSpell.soundEffect = custData["Sound Effects"];

                                //cardSpell.animEffect = custData["Animation Effects"];

                                if (int.TryParse(custData["Soul Dust Purchase Cost"], out tempInt))
                                    cardSpell.PurchaseCost = tempInt;

                                cardSpell.Rarity = custData["Rank"];

                                if (int.TryParse(custData["Mana Cost"], out tempInt))
                                    cardSpell.manaCost = tempInt;

                                cardSpell.info = custData["Info"];

                                cardSpell.Effect = custData["Effect"];

                                catalog.Add(cardSpell);

                                break;

                            case "weapon":

                                CatalogCardWeapon newWeapon = new CatalogCardWeapon();   //  (CatalogCardCreature) temp;

                                newWeapon = (CatalogCardWeapon)InitItem(newWeapon, catItem);

                                newWeapon.catalogItemType = typeof(CatalogCardWeapon);

                                custData = PlayFab.Json.JsonWrapper.DeserializeObject<Dictionary<string, string>>(catItem.CustomData);

                                if (float.TryParse(custData["Version"], out tempVer))
                                    newWeapon.itemVersion = tempVer;

                                if (int.TryParse(custData["Soul Dust Disenchant Cost"], out tempInt))
                                    newWeapon.DisenchantCost = tempInt;

                                //newWeapon.soundEffect = custData["Sound Effects"];

                                //newWeapon.animEffect = custData["Animation Effects"];

                                if (int.TryParse(custData["Soul Dust Purchase Cost"], out tempInt))
                                    newWeapon.PurchaseCost = tempInt;

                                if ((custData.TryGetValue("disenchantable", out tempStr)))
                                    bool.TryParse(custData["disenchantable"], out tempBool);
                                else
                                    tempBool = true;

                                ((CatalogBaseCard)newWeapon).disenchantable = tempBool;

                                if (int.TryParse(custData["Mana Cost"], out tempInt))
                                    newWeapon.manaCost = tempInt;

                                if (int.TryParse(custData["HP"], out tempInt))
                                    newWeapon.HP = tempInt;

                                newWeapon.skill = custData["Skill"];

                                if (int.TryParse(custData["ATK"], out tempInt))
                                    newWeapon.attack = tempInt;

                                catalog.Add(newWeapon);
                                break;
                            default:
                                break;
                        }


                        break;

                    case "bundle":
                        CatalogBundle newBundle = new CatalogBundle();

                        newBundle = (CatalogBundle)InitItem(newBundle, catItem);

                        newBundle.catalogItemType = typeof(CatalogBundle);

                        newBundle.RandomValues = catItem.Bundle.BundledResultTables;

                        newBundle.VirtualCurrencies = catItem.Bundle.BundledVirtualCurrencies;

                        foreach (string bundleItem in catItem.Bundle.BundledItems)
                        {
                            newBundle.bundleItems.Add(bundleItem, GetCatalogItemById(bundleItem));
                        }

                        catalog.Add(newBundle);

                        break;
                    default:

                        CatalogGenericItem tempGenericItem = new CatalogGenericItem();

                        tempGenericItem = (CatalogGenericItem)InitItem(tempGenericItem, catItem);

                        catalog.Add(tempGenericItem);
                        Console.WriteLine("Error on catalog items: " + catItem.DisplayName + " class not found: " + catItem.ItemClass);
                        break;
                }
            }

            if (_callback != null)
                _callback(new BackendInitResult(true,null,false));

            return true;

        }


        public async System.Threading.Tasks.Task<bool> GetInventory(COFPlayerProfile player)
        {

            ICatalogItem tempItem;
            bool tempBool;
            string tempStr;
            String[] itemClassLevels;

            GetUserInventoryRequest inventoryRequest = new GetUserInventoryRequest();
            inventoryRequest.PlayFabId = player.playfabId;

            PlayFabResult<GetUserInventoryResult> inventoryResult = await PlayFabServerAPI.GetUserInventoryAsync(inventoryRequest);

            Console.WriteLine("Inventory items: " + inventoryResult.Result.Inventory.Count);

            player.inventory.Clear();

            foreach (ItemInstance invItem in inventoryResult.Result.Inventory)
            {

                tempItem = GetCatalogItemById(invItem.ItemId);

                tempItem.itemInstanceID = invItem.ItemInstanceId;

                itemClassLevels = invItem.ItemClass.Split(':');

                switch (itemClassLevels[0])
                {
                    case "card":

                        if ((invItem.CustomData != null) && (invItem.CustomData.TryGetValue("disenchantable", out tempStr)))
                            bool.TryParse(invItem.CustomData["disenchantable"], out tempBool);
                        else
                            tempBool = true;

                        ((CatalogBaseCard)tempItem).disenchantable = tempBool;

                        break;

                    default:
                        break;
                }

                player.inventory.Add(tempItem);

            }

            return true;
        }
        private void AfterGetCatalog()
        {

            ICatalogItem tempItem;
            Dictionary<string, string> custData;
            float tempVer;
            int tempInt;
            string tempStr;
            bool tempBool = true;
            String[] itemClassLevels;

            Console.WriteLine("Catalog loaded. Items found: " + catalog.Count);

            // Inventory

            Console.WriteLine("Inventory items: " + inventory.Count);

            foreach (ItemInstance invItem in inventory)
            {

                tempItem = GetCatalogItemById(invItem.ItemId);

                tempItem.itemInstanceID = invItem.ItemInstanceId;

                itemClassLevels = invItem.ItemClass.Split(':');

                switch (itemClassLevels[0])
                {
                    case "card":

                        if ((invItem.CustomData != null) && (invItem.CustomData.TryGetValue("disenchantable", out tempStr)))
                            bool.TryParse(invItem.CustomData["disenchantable"], out tempBool);
                        else
                            tempBool = true;

                        ((CatalogBaseCard)tempItem).disenchantable = tempBool;

                        break;

                    default:
                        break;
                }

                inventory.Add(tempItem);

            }


            // Creation of Deck

            String[] deckCards = PlayFab.Json.JsonWrapper.DeserializeObject<string[]>(deckString);
            ICatalogItem tempDeckItem;

            deck.Clear();

            foreach (string deckCardInventoryId in deckCards)
            {
                tempDeckItem = GetInventoryItemByInstanceId(deckCardInventoryId);

                if (tempDeckItem != null)
                {
                    deck.Add(tempDeckItem);
                }
                else
                {
                    Console.WriteLine("Deck creation Error: ItemInstanceId " + deckCardInventoryId + " not found in Inventory");
                }

            }

            // End of initalization
            // Signal the caller that we have finished intialization with the passes callback
            if (_callback != null)
            {
                _callback(new BackendInitResult(true, null, isNewlyCreated));
            }

        }

        private ICatalogItem InitItem(ICatalogItem item, CatalogItem catItem)
        {

            item.displayName = catItem.DisplayName;
            item.itemID = catItem.ItemId;
            item.tags = catItem.Tags;
            item.itemImageURL = catItem.ItemImageUrl;
            item.description = catItem.Description;
            item.isStackable = catItem.IsStackable;

            return item;

        }

        private void AfterGetInventory()
        {
            // End of initalization
            // Signal the caller that we have finished intialization with the passes callback
            Console.WriteLine("Get Inventory Executed Succesfully. Total items: " + inventory.Count);
            if (_callback != null)
            {
                _callback(new BackendInitResult(true, null, isNewlyCreated));
            }
        }

        //private static ICatalogItem GetCatalogItemClone(string id)
        //{
        //    foreach (ICatalogItem singleItem in BackendManager.Instance.catalog)
        //    {
        //        if (singleItem.itemID == id)
        //            return (ICatalogItem)COFUtils.CloneObject(singleItem, singleItem.catalogItemType);
        //    }

        //    return null;
        //}

        private static ICatalogItem GetCatalogItemById(string id)
        {
            foreach (ICatalogItem singleItem in BackendManager.Instance.catalog)
            {
                if (singleItem.itemID == id)
                    return singleItem;
            }

            return null;
        }

        private static ICatalogItem GetInventoryItemByInstanceId(string instanceId)
        {
            foreach (ICatalogItem singleItem in BackendManager.Instance.inventory)
            {
                if (singleItem.itemInstanceID == instanceId)
                    return singleItem;
            }

            return null;
        }


    }

}
