﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using COF.Base;

namespace COF.Server
{
    public class CatalogGenericItem : ICatalogItem
    {
        string _description = null;
        public string description
        {
            get
            {
                if (_description == null)
                {
                    // get value from playfab
                }

                return _description;
            }

            set
            {
                _description = value;
            }
        }

        string _displayName;
        public string displayName
        {
            get
            {
                return _displayName;
            }

            set
            {
                _displayName = value;
            }
        }

        bool _isStackable;
        public bool isStackable
        {
            get
            {
                return _isStackable;
            }

            set
            {
                _isStackable = value;
            }
        }

        string _itemClass;
        public string itemClass
        {
            get
            {
                return _itemClass;
            }

            set
            {
                _itemClass = value;
            }
        }

        string _itemID;
        public string itemID
        {
            get
            {
                return _itemID;
            }

            set
            {
                _itemID = value;
            }
        }

        string _itemImageURL;
        public string itemImageURL
        {
            get
            {
                return _itemImageURL;
            }

            set
            {
                _itemImageURL = value;
            }
        }

        List<string> _tags;
        public List<string> tags
        {
            get
            {
                return _tags;
            }

            set
            {
                _tags = value;
            }
        }

        float _itemVersion;

        public float itemVersion
        {
            get
            {
                return _itemVersion;
            }

            set
            {
                _itemVersion = value;
            }
        }

        Type _catalogItemType;
        public Type catalogItemType
        {
            get
            {
                return _catalogItemType;
            }

            set
            {
                _catalogItemType = value;
            }
        }

        string _itemInstanceID;
        public string itemInstanceID
        {
            get
            {
                return _itemInstanceID;
            }

            set
            {
                _itemInstanceID = value;
            }
        }
    }
}
