﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace COF.Server
{
    public class CatalogBaseCard : CatalogGenericItem
    {
        public string itemDescription { get; internal set; }
        public string Rarity { get; internal set; }
        public int DisenchantCost { get; internal set; }
        public int manaCost { get; internal set; }
        public int PurchaseCost { get; internal set; }
        public string animEffect { get; internal set; }
        public string soundEffect { get; internal set; }
        public bool disenchantable { get; internal set; }
    }
}
