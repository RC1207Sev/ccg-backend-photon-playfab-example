﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace COF.Server
{
    class BackendInitResult
    {

        public bool isSuccess;
        public string errorMessage;
        public bool isNewPlayer;

        public BackendInitResult(bool pIsSuccess, string pErrorMessage, bool pIsNewPlayer)
        {
            isSuccess = pIsSuccess;
            errorMessage = pErrorMessage;
            isNewPlayer = pIsNewPlayer;
        }

    }
}
